<?php


namespace app\modules\upup\services;


use app\components\operatorApi\LocalOperatorApi;
use app\exceptions\BadRequestToService;
use app\modules\upup\components\RequestUrl;
use app\modules\upup\events\interfaces\ISellingEvent;
use app\modules\upup\events\order\OrderSellingEvent;
use app\modules\upup\helpers\OrderStatusHelper;
use app\modules\upup\models\orders\providers\OrderSellDataProvider;
use app\modules\upup\repositories\order\OrderRepository;
use app\modules\upup\repositories\order\OrderUppRepository;
use app\modules\upup\services\exceptions\CarAssignException;
use app\repositories\exceptions\NotFoundException;
use Yii;
use yii\base\Event;
use yii\httpclient\Response;

class OrderSellingService extends ServiceBase implements ISellingEvent
{
    const ADD_ACTION = 'add';
    const UPDATE_ACTION = 'edit';
    const DELETE_ACTION = 'del';
    const INFO_ACTION = 'info';

    /**
     * @var RequestUrl
     */
    private $requestUrl;
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var OrderUppRepository
     */
    private $upupRepository;

    public function __construct(
        RequestUrl $requestUrl,
        OrderRepository $orderRepository,
        OrderUppRepository $upupRepository
    ) {
        $this->requestUrl = $requestUrl;
        $this->orderRepository = $orderRepository;
        $this->upupRepository = $upupRepository;
    }

    public function add(int $orderId)
    {
        $order = $this->orderRepository->getById($orderId);
        $data = array_merge($this->getCommonParams(self::ADD_ACTION), (new OrderSellDataProvider($order))->getData());

        $response = $this->createRequest($data);

        if (!$response->isOk) {
            $this->sendErrorMessage('Error to add the order to exchange', $response->toString(), self::ADD_ACTION);

            throw new BadRequestToService('The Upup service has returned the http status code: ' . $response->getStatusCode());
        }

        $responseData = $response->getData();

        Event::trigger($this, self::ADD_EVENT, (new OrderSellingEvent([
            'upupOrderId' => $responseData['id'],
            'orderId'     => $orderId,
        ])));
    }

    public function update(int $orderId)
    {
        $order = $this->orderRepository->getById($orderId);
        $upupOrder = $this->upupRepository->getById($orderId);
        $data = array_merge($this->getCommonParams(self::UPDATE_ACTION), (new OrderSellDataProvider($order))->getData(),
            ['id' => $upupOrder->upup_id]);

        $response = $this->createRequest($data);

        if (!$response->isOk) {
            $this->sendErrorMessage('Error to edit the order in the exchange', $response->toString(),
                self::UPDATE_ACTION);

            throw new BadRequestToService('The Upup service has returned the http status code: ' . $response->getStatusCode());
        }

        $responseData = $response->getData();

        if ($responseData['result'] === 'error' && $responseData['error'] === 'already dispatched') {
            throw new CarAssignException('already dispatched');
        }

        Event::trigger($this, self::UPDATE_EVENT, (new OrderSellingEvent([
            'upupOrderId' => $responseData['id'],
            'orderId'     => $orderId,
        ])));
    }

    public function delete(int $orderId)
    {
        $upupOrder = $this->upupRepository->getById($orderId);
        $data = array_merge($this->getCommonParams(self::DELETE_ACTION), ['id' => $upupOrder->upup_id]);

        $response = $this->createRequest($data);

        if (!$response->isOk) {
            $this->sendErrorMessage('Error to edit the order in the exchange', $response->toString(),
                self::DELETE_ACTION);

            throw new BadRequestToService('The Upup service has returned the http status code: ' . $response->getStatusCode());
        }

        Event::trigger($this, self::DELETE_EVENT, (new OrderSellingEvent([
            'orderId' => $orderId,
        ])));
    }

    public function getInfo(int $orderId, int $check = 0)
    {
        $data = array_merge($this->getCommonParams(self::INFO_ACTION), [
            'external_id' => $orderId,
            'check'       => $check,
        ]);

        $response = $this->createRequest($data);

        if (!$response->isOk) {
            $this->sendErrorMessage('Error of getting info for the order with id: ' . $orderId, $response->toString(),
                self::INFO_ACTION);

            throw new BadRequestToService('The Upup service has returned the http status code: ' . $response->getStatusCode());
        }

        return $response->getData();
    }

    public function changeStatus(string $orderid, string $status, int $tenantId): ?string
    {
        if (!$this->orderRepository->isExist($orderid)) {
            throw new NotFoundException('The order is not found');
        }

        $statusId = OrderStatusHelper::getStatus($status);

        if (is_null($statusId)) {
            return null;
        }

        $params = [
            'fields' => [
                'status_id' => $statusId,
            ],
        ];

        /** @var $operatorApi LocalOperatorApi */
        $operatorApi = Yii::$app->get('operatorApi');

        return $operatorApi->createUpdateEvent($tenantId, null, $orderid, $params, null);
    }

    /**
     * @param array $data
     * @return Response
     */
    private function createRequest(array $data): Response
    {
        $response = $this->getHttpClient()
            ->createRequest()
            ->setMethod('post')
            ->setUrl($this->requestUrl->getUrl())
            ->setData($data)
            ->send();

        return $response;
    }

    private function sendErrorMessage(string $message, string $responseMessage, string $action)
    {
        Yii::error([
            'message'      => $message,
            'url'          => $this->requestUrl->getUrl(),
            'upup_action'  => $action,
            'httpResponse' => $responseMessage,
        ], 'upup');
    }

    private function getCommonParams(string $action): array
    {
        return [
            'v'          => $this->requestUrl->getVersion(),
            'key'        => $this->requestUrl->getApikey(),
            'character'  => 'utf-8',
            'controller' => 'order',
            'function'   => $action,
            'json'       => 1,
        ];
    }
}