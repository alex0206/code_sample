<?php


namespace app\modules\upup\services;


use app\components\xml\builder\XmlBuilder;
use app\modules\upup\components\RequestUrl;
use app\modules\upup\helpers\DriverStatusHelper;
use app\modules\upup\models\drivers\providers\DataProvider;
use app\modules\upup\models\drivers\providers\TrackDataProvider;
use app\modules\upup\repositories\CabRepository;
use Yii;

class DriverService extends ServiceBase
{
    /**
     * @var CabRepository
     */
    private $cabRepository;

    public function __construct(CabRepository $cabRepository)
    {
        $this->cabRepository = $cabRepository;
    }

    public function exportDrivers(): array
    {
        $cabs = $this->getAllCabs();

        return (new DataProvider($cabs))->getData();
    }

    public function sendStatus(string $callsign, string $status, RequestUrl $requestUrl): bool
    {
        $cab = $this->cabRepository->getOne($callsign);
        $url = $requestUrl->getUrl();
        $response = $this->getHttpClient()->get($url, [
            'v'      => $requestUrl->version,
            'clid'   => $requestUrl->clid,
            'apikey' => $requestUrl->apikey,
            'uuid'   => $cab->getId(),
            'status' => DriverStatusHelper::getExchangeStatus($status),
        ])
            ->send();

        if (!$response->isOk) {
            Yii::error([
                'message'      => 'Error to change status in up&up exchange',
                'url'          => $url,
                'httpResponse' => $response->toString(),
            ], 'upup');

            return false;
        }

        return true;
    }

    public function sendLocations(RequestUrl $requestUrl)
    {
        /** @var XmlBuilder $xmlBuilder */
        $xmlBuilder = Yii::$app->get('xmlBuilder');
        $xmlBuilder->rootTag = 'tracks';
        $xmlBuilder->rootAttributes = [
            ['clid', $requestUrl->clid],
            ['v' => $requestUrl->version],
        ];

        $cabs = $this->getAllCabs();

        if (empty($cabs)) {
            return null;
        }

        $locationData = (new TrackDataProvider($cabs))->getData();

        $xmlData = $xmlBuilder->build($locationData);

        $url = $requestUrl->getUrl();

        $response = $this->getHttpClient()
            ->createRequest()
            ->addHeaders(['Content-Type' => 'multipart/form-data'])
            ->setMethod('post')
            ->setUrl($url)
            ->setData(['compressed' => 0, 'data' => $xmlData])
            ->send();

        if (!$response->isOk) {
            Yii::error([
                'message'      => 'Error to send the driver geolocation data',
                'url'          => $url,
                'httpResponse' => $response->toString(),
            ], 'upup');

            return false;
        }

        return true;
    }

    /**
     * @return array Cab[]
     */
    private function getAllCabs(): array
    {
        return $this->cabRepository->getAll();
    }
}