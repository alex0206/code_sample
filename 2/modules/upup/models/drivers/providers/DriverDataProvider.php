<?php


namespace app\modules\upup\models\drivers\providers;


use app\modules\upup\models\drivers\Cab;
use app\modules\upup\models\interfaces\IDataProvider;
use app\repositories\exceptions\NotFoundException;
use app\repositories\interfaces\IDocumentsRepository;
use RuntimeException;

class DriverDataProvider implements IDataProvider
{
    /**
     * @var Cab
     */
    private $cab;
    /**
     * @var IDocumentsRepository
     */
    private $documentsRepository;

    public function __construct(Cab $cab, IDocumentsRepository $documentsRepository)
    {
        $this->cab = $cab;
        $this->documentsRepository = $documentsRepository;
    }

    public function getData(): array
    {
        $driverData = $this->cab->getDriver();

        return [
            'DisplayName'   => trim($driverData['last_name'] . $driverData['name'] . $driverData['second_name']),
            'Phone'         => $driverData['phone'],
            'DriverLicense' => $this->getDriverLicense($driverData['worker_id']),
        ];
    }

    private function getDriverLicense(int $driverId): string
    {
        try {
            $licenseData = $this->documentsRepository->getDriverLicense($driverId);

            return $licenseData['series'] . $licenseData['number'];
        } catch (NotFoundException $e) {
            throw new RuntimeException($e->getMessage());
        }
    }
}