<?php


namespace app\modules\upup\models\drivers\providers;


use app\modules\upup\helpers\CarOptionHelper;
use app\modules\upup\models\drivers\Cab;
use app\modules\upup\models\interfaces\IDataProvider;
use Yii;
use yii\helpers\ArrayHelper;

class CarDataProvider implements IDataProvider
{
    /**
     * @var Cab
     */
    private $cab;

    public function __construct(Cab $cab)
    {
        $this->cab = $cab;
    }

    public function getData(): array
    {
        $carData = $this->cab->getCar();

        $carDetails = [
            'Model'     => $carData['name'],
            'Color'     => $this->getColor($carData['color']),
            'CarNumber' => strtoupper($carData['gos_number']),
            'Age'       => $carData['year'],
            'Permit'    => $carData['license'],
        ];

        $carOptions = $this->getCarOptions($carData['carHasOptions']);

        return ArrayHelper::merge($carDetails, $carOptions);
    }

    private function getColor(string $color): string
    {
        return $color ? Yii::t('car', $color, [], 'ru-RU') : '';
    }

    private function getCarOptions(array $carHasOptions): array
    {
        $result = [];
        $carOptions = ArrayHelper::getColumn($carHasOptions, 'option_id');
        $exchangeOptions = CarOptionHelper::getOptionKeys();

        foreach ($exchangeOptions as $exchangeOption) {
            $result[] = [
                'tag'        => 'Require',
                'attributes' => ['name' => $exchangeOption],
                'value'      => CarOptionHelper::checkOption($exchangeOption, $carOptions),
            ];
        }

        return $result;
    }
}