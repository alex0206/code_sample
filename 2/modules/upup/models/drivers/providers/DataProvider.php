<?php


namespace app\modules\upup\models\drivers\providers;


use app\modules\upup\models\drivers\Cab;
use app\modules\upup\models\interfaces\IDataProvider;
use app\repositories\interfaces\IDocumentsRepository;
use Yii;


class DataProvider implements IDataProvider
{
    /**
     * @var Cab[]
     */
    private $cabs;

    public function __construct(array $cabs)
    {
        $this->cabs = $cabs;
    }

    public function getData(): array
    {
        $exportData = [];
        /** @var IDocumentsRepository $documentsRepository */
        $documentsRepository = Yii::$container->get('app\repositories\interfaces\IDocumentsRepository');

        foreach ($this->cabs as $cab) {
            if (!($cab instanceof Cab)) {
                continue;
            }

            $carProvider = new CarDataProvider($cab);
            $driverProvider = new DriverDataProvider($cab, $documentsRepository);

            $exportData['Car'] = [
                'Uuid'          => $cab->getId(),
                'CarDetails'    => $carProvider->getData(),
                'DriverDetails' => $driverProvider->getData(),
            ];
        }

        return $exportData;
    }
}