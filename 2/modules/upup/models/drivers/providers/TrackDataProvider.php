<?php


namespace app\modules\upup\models\drivers\providers;


use app\modules\upup\models\drivers\Cab;
use app\modules\upup\models\interfaces\IDataProvider;
use Yii;

class TrackDataProvider implements IDataProvider
{
    /**
     * @var Cab[]
     */
    private $cabs;

    public function __construct(array $cabs)
    {
        $this->cabs = $cabs;
    }

    public function getData(): array
    {
        $arResult = [];

        foreach ($this->cabs as $cab) {
            $geoInfo = $cab->getGeo();
            $arResult[] = [
                [
                    'tag'        => 'track',
                    'attributes' => ['uuid' => $cab->getId()],
                    'value'      => [
                        [
                            'tag'        => 'point',
                            'attributes' => [
                                'latitude'  => $geoInfo['lat'],
                                'longitude' => $geoInfo['lon'],
                                'avg_speed' => $geoInfo['speed'],
                                'direction' => $geoInfo['degree'],
                                'time'      => Yii::$app->formatter->asDate($geoInfo['update_time'], 'ddMMy:HHmmss'),
                            ],
                        ],
                    ],
                ],
            ];
        }

        return $arResult;
    }
}