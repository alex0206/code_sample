<?php


namespace app\modules\upup\models\orders\providers;


use app\modules\upup\helpers\OrderOptionHelper;
use app\modules\upup\models\interfaces\IDataProvider;
use app\modules\upup\models\orders\Address;
use app\modules\upup\models\orders\Order;
use app\modules\upup\models\tariff\TariffExchangeConverter;
use app\modules\upup\repositories\TariffRepository;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class OrderSellDataProvider
 * Getting data for selling order to exchange
 * @package app\modules\upup\models\orders\providers
 */
class OrderSellDataProvider implements IDataProvider
{
    /**
     * @var Order
     */
    private $order;
    private $options = null;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function getData(): array
    {
        $address = new Address($this->order->address);

        return [
            'deadline'    => $this->getDeadline(),
            'number'      => $this->order->client->phone->value,
            'name'        => $this->order->client->getFullName(),
            'city'        => $this->order->city->name,
            'street'      => $address->getStreet(),
            'home'        => $address->getBuilding(),
            'structure'   => $address->getHousing(),
            'porch'       => $address->getPorch(),
            'tarif'       => $this->getTariff(),
            'comment'     => $this->order->comment,
            'card'        => OrderOptionHelper::hasOption('card', $this->getOptions()),
            'children'    => OrderOptionHelper::hasOption('children', $this->getOptions()),
            'nosmoking'   => OrderOptionHelper::hasOption('nosmoking', $this->getOptions()),
            'animal'      => OrderOptionHelper::hasOption('animal', $this->getOptions()),
            'conditioner' => OrderOptionHelper::hasOption('conditioner', $this->getOptions()),
            'external_id' => $this->order->order_id,
            'lat'         => $address->getLat(),
            'lon'         => $address->getLon(),
        ];
    }

    /**
     * Getting order time by unix timestamp without city offset
     * @return int
     */
    private function getDeadline(): int
    {
        return $this->order->order_time - $this->order->time_offset;
    }

    private function getOptions(): array
    {
        if (is_null($this->options)) {
            $this->options = ArrayHelper::getColumn($this->order->options, 'option_id', false);
        }

        return $this->options;
    }

    private function getTariff(): string
    {
        /** @var TariffRepository $tariffRepository */
        $tariffRepository = Yii::createObject(TariffRepository::class, [$this->order->tenant_id]);

        $tariffConverter = new TariffExchangeConverter($this->order->order_id, $tariffRepository);

        return $tariffConverter->getTariff();
    }
}