<?php

namespace app\modules\upup\models\orders;

use app\models\order\Order;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%order_upup}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $upup_id
 *
 * @property Order $order
 */
class OrderUpup extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_upup}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['order_id', 'upup_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'       => 'ID',
            'order_id' => 'Order ID',
            'upup_id'  => 'Upup ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }
}
