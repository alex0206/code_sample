<?php


namespace app\modules\upup\models\orders;

use app\models\city\City;
use app\models\client\Client;
use app\models\order\OrderHasOption;

/**
 * Class Order
 * @package app\modules\upup\models\orders
 * @property OrderUpup $upup
 * @property Client $client
 * @property City $city
 * @property OrderHasOption[] $options
 */
class Order extends \app\models\order\Order
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpup()
    {
        return $this->hasOne(OrderUpup::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptions()
    {
        return $this->hasMany(OrderHasOption::className(), ['order_id' => 'order_id']);
    }
}