<?php


namespace app\modules\upup\models\orders;


use SebastianBergmann\GlobalState\RuntimeException;

class Address
{
    /**
     * @var array
     */
    private $address;

    public function __construct(string $gootaxAddress)
    {
        $this->setAddress($gootaxAddress);
    }

    public function getStreet(): string
    {
        if (!isset($this->address['street'])) {
            throw new RuntimeException('The street of order is empty');
        }

        return $this->address['street'];
    }

    public function getBuilding(): ?string
    {
        return $this->getAdressPart('house');
    }

    public function getHousing(): ?string
    {
        return $this->getAdressPart('housing');
    }

    public function getPorch(): ?string
    {
        return $this->getAdressPart('porch');
    }

    public function getLat(): ?string
    {
        return $this->getAdressPart('lat');
    }

    public function getLon(): ?string
    {
        return $this->getAdressPart('lon');
    }

    private function getAdressPart(string $addressPart): ?string
    {
        return $this->address[$addressPart] ?? null;
    }

    private function setAddress(string $gootaxAddress)
    {
        $address = unserialize($gootaxAddress);

        $this->address = current($address);
    }
}