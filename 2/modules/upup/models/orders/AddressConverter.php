<?php


namespace app\modules\upup\models\orders;

use yii\helpers\ArrayHelper;


/**
 * Class AddressConverter
 * Convert exchange address data to gootax address
 * @package app\modules\upup\models\orders\
 */
class AddressConverter
{
    /**
     * @var array
     */
    private $firstPoint;
    /**
     * @var array
     */
    private $destinations;
    /**
     * @var int
     */
    private $cityId;

    public function __construct(array $firstPoint, array $destinations, int $cityId)
    {
        $this->firstPoint = $firstPoint;
        $this->destinations = $destinations;
        $this->cityId = $cityId;
    }

    public function getData(): string
    {
        $points = $this->getPoints();
        $char = 'A';
        $address = [];

        foreach ($points as $point) {
            $address[$char] = [
                'city'       => $point['Country']['Locality']['LocalityName'],
                'city_id'    => $this->cityId,
                'street'     => $point['Country']['Locality']['Thoroughfare']['ThoroughfareName'],
                'house'      => $point['Country']['Locality']['Thoroughfare']['Premise']['PremiseNumber'],
                'lat'        => $point['Point']['Lat'],
                'lon'        => $point['Point']['Lon'],
                'parking'    => '',
                'parking_id' => '',
                'housing'    => '',
                'porch'      => $point['Country']['Locality']['Thoroughfare']['Premise']['PorchNumber'] ?? '',
                'apt'        => '',
            ];

            $char++;
        }

        return serialize($address);
    }

    private function getDistanations(): array
    {
        $destinations = $this->destinations;
        if (!empty($this->destinations) && !ArrayHelper::isIndexed($this->destinations)) {
            $destinations = [$this->destinations];
        }

        return $destinations;
    }

    private function getPoints(): array
    {
        $destinations = $this->getDistanations();

        return array_merge($this->firstPoint, $destinations);
    }
}