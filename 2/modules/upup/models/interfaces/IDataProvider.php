<?php

namespace app\modules\upup\models\interfaces;

interface IDataProvider
{
    public function getData(): array;
}