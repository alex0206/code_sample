<?php


namespace app\modules\upup\models\tariff;


use app\modules\upup\repositories\TariffRepository;

class TariffExchangeConverter
{
    const FIX_TARIFF = 'f';
    const TIME_TARIFF = 'm';
    const DISTANCE_TARIFF = 'k';
    /**
     * @var TariffRepository
     */
    private $repository;
    /** @var array All data */
    private $data;
    /** @var array Tariff data based on location */
    private $tariffData;

    public function __construct(int $orderId, TariffRepository $repository)
    {
        $this->repository = $repository;
        $this->data = $this->repository->getTariff($orderId);
        $this->setTariffData();
    }

    public function getTariff(): string
    {
        return implode('/', $this->getTariffParams());
    }

    private function getTariffParams(): array
    {
        $tariffing = $this->getTariffing();
        $tariffParams = [
            $this->getClass(),
            $tariffing,
        ];

        if ($tariffing === self::FIX_TARIFF) {
            $tariffParams = array_merge($tariffParams, [
                $this->getSummaryCost(),
            ]);
        } else {
            $tariffParams = array_merge($tariffParams, [
                $this->getMinPrice(),
                $this->getPlantingInclude(),
                $this->getNextPrice(),
                $this->getFreeWaitTime(),
                $this->getWaitPrice(),
                $this->getCountrysidePrice(),
                $this->getAdditioanalCostOfForcedDowntime(),
                $this->getSpeedPaidWaiting(),
            ]);
        }

        return $tariffParams;
    }

    private function getClass(): string
    {
        return CarClassMapper::getCarClass($this->data['tariff']['class_id']);
    }

    private function getTariffing(): string
    {
        $accrual = $this->getAccrual();

        switch ($accrual) {
            case 'FIX':
                return self::FIX_TARIFF;
            case 'TIME':
                return self::TIME_TARIFF;
            default:
                return self::DISTANCE_TARIFF;
        }
    }

    private function isCity(): bool
    {
        return $this->data['costData']['start_point_location'] === 'in';
    }

    private function isDay(): bool
    {
        return $this->data['costData']['tariffInfo']['isDay'] == 1;
    }

    private function setTariffData()
    {
        $tariffDataLocationKey = $this->isCity() ? 'tariffDataCity' : 'tariffDataTrack';
        $this->tariffData = $this->data['costData']['tariffInfo'][$tariffDataLocationKey];
    }

    private function getAccrual(): string
    {
        return $this->tariffData['accrual'];
    }

    private function getSummaryCost()
    {
        return $this->data['costData']['summary_cost'];
    }

    private function getMinPrice(): string
    {
        if (!$this->isAllowDayNignt()) {
            return $this->tariffData['min_price_day'];
        }

        return $this->isDay() ? $this->tariffData['min_price_day'] : $this->tariffData['min_price_night'];
    }

    private function getPlantingInclude(): string
    {
        if (!$this->isAllowDayNignt()) {
            return $this->tariffData['planting_include_day'];
        }

        return $this->isDay() ? $this->tariffData['planting_include_day'] : $this->tariffData['planting_include_night'];
    }

    private function getNextPrice(): string
    {
        if ($this->getAccrual() == 'INTERVAL') {
            if (!$this->isAllowDayNignt()) {
                $intervalData = $this->tariffData['next_km_price_day'];
            } else {
                $intervalData = $this->isDay() ? $this->tariffData['next_km_price_day'] : $this->tariffData['next_km_price_night'];
            }

            $intervalData = unserialize($intervalData);

            return $intervalData[0]['price'];
        }

        if (!$this->isAllowDayNignt()) {
            return $this->tariffData['next_km_price_day'];
        }

        return $this->isDay() ? $this->tariffData['next_km_price_day'] : $this->tariffData['next_km_price_night'];
    }

    private function getCountrysidePrice(): string
    {
        $tariffData = $this->data['costData']['tariffInfo']['tariffDataTrack'];

        if (!$this->isAllowDayNignt($tariffData)) {
            return $tariffData['next_km_price_day'];
        }

        return $this->isDay() ? $tariffData['next_km_price_day'] : $tariffData['next_km_price_night'];
    }

    private function isAllowDayNignt(?array $tariffData = null): bool
    {
        $data = $tariffData ?? $this->tariffData;

        return $data['allow_day_night'] == 1;
    }

    private function getFreeWaitTime(): string
    {
        if (!$this->isAllowDayNignt()) {
            return $this->tariffData['wait_time_day'];
        }

        return $this->isDay() ? $this->tariffData['wait_time_day'] : $this->tariffData['wait_time_night'];
    }

    private function getWaitPrice(): string
    {
        if (!$this->isAllowDayNignt()) {
            return $this->tariffData['wait_price_day'];
        }

        return $this->isDay() ? $this->tariffData['wait_price_day'] : $this->tariffData['wait_price_night'];
    }

    private function getSpeedPaidWaiting(): string
    {
        if (!$this->isAllowDayNignt()) {
            return $this->tariffData['speed_downtime_day'];
        }

        return $this->isDay() ? $this->tariffData['speed_downtime_day'] : $this->tariffData['speed_downtime_night'];
    }

    private function getAdditioanalCostOfForcedDowntime(): string
    {
        return $this->getWaitPrice();
    }
}