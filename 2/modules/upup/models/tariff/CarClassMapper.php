<?php


namespace app\modules\upup\models\tariff;


use yii\base\NotSupportedException;

class CarClassMapper
{
    public static function getCarClass(int $carClassId): string
    {
        switch ($carClassId) {
            case 1:
                return 'e';
            case 2:
                return 'c';
            case 3:
                return 'b';
            case 5:
                return 'v';
            case 6:
                return 'u';
            case 7:
                return 'a';
            default:
                throw new NotSupportedException('Not suppoted the car class for upup exchange');
        }
    }
}