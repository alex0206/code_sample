<?php


namespace app\modules\upup\helpers;


class ExchangeDriverStatus
{
    const FREE = 'free';
    const BUSY = 'busy';
    const VERY_BUSY = 'verybusy';
}