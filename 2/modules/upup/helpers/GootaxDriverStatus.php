<?php


namespace app\modules\upup\helpers;


class GootaxDriverStatus
{
    const FREE = 'FREE';
    const BUSY = 'ON_ORDER';
}