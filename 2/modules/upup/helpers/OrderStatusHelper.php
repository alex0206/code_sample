<?php


namespace app\modules\upup\helpers;


class OrderStatusHelper
{
    public static function getStatus(string $exchangeStatus): ?int
    {
        switch ($exchangeStatus) {
            case 'carassigned':
                return 17;
            case 'cararrived':
                return 26;
            case 'clienttransporting':
                return 36;
            case 'complete':
                return 36;
            case 'failed':
                return 36;
            case 'cancelled':
                return 36;
            default:
                return null;
        }
    }
}