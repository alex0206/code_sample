<?php


namespace app\modules\upup\helpers;


use yii\helpers\ArrayHelper;

class CarOptionHelper
{
    public static function getOptionMap(): array
    {
        return [
            'animal_transport' => 8,
            'card'             => 6,
            'child_chair'      => 4,
            'conditioner'      => 1,
            'no_smoking'       => 15,
        ];
    }

    /**
     * @param string $optionName Exchange option name
     * @param array $carOptions Gootax car`s options
     * @return string
     */
    public static function checkOption($optionName, $carOptions): string
    {
        $optionMap = self::getOptionMap();

        return in_array($optionMap[$optionName], $carOptions) ? 'yes' : 'no';
    }

    public static function getOptionKeys(): array
    {
        return array_keys(self::getOptionMap());
    }

    /**
     * @param string $exchangeStatus
     * @return mixed
     */
    public static function getOptionId(string $exchangeStatus)
    {
        return ArrayHelper::getValue(self::getOptionMap(), $exchangeStatus);
    }
}