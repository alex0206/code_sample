<?php


namespace app\modules\upup\helpers;


use app\modules\upup\components\RequestUrl as Url;
use app\modules\upup\Module;
use yii\rest\Controller;

/**
 * Class RequestUrl
 * @package app\modules\upup\helpers
 * @mixin Controller
 */
trait RequestUrl
{
    public function getRequestUrl(?string $action = null): Url
    {
        /** @var Module $module */
        $module = $this->module;

        $requestUrl = $module->requestUrl
            ->setVersion($module->protocolVersion)
            ->setApiKey($module->getApiKey())
            ->setClid($module->getClid());

        if (!empty($action)) {
            $requestUrl->setAction($action);
        }

        return $requestUrl;
    }
}