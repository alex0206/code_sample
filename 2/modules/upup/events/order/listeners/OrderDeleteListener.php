<?php


namespace app\modules\upup\events\order\listeners;


use app\modules\upup\events\interfaces\IEventListener;
use app\modules\upup\events\order\OrderSellingEvent;
use app\modules\upup\repositories\order\OrderUppRepository;

class OrderDeleteListener implements IEventListener
{
    /**
     * @var OrderUppRepository
     */
    private $repository;

    public function __construct(OrderUppRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param OrderSellingEvent $event
     */
    public function handle($event)
    {
        $this->repository->delete($event->orderId);
    }
}