<?php


namespace app\modules\upup\events\order\listeners;


use app\modules\upup\events\interfaces\IEventListener;
use app\modules\upup\events\order\OrderSellingEvent;
use app\modules\upup\models\orders\OrderUpup;
use app\modules\upup\repositories\order\OrderUppRepository;


class OrderSoldListener implements IEventListener
{
    /**
     * @var OrderUppRepository
     */
    private $repository;

    public function __construct(OrderUppRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param OrderSellingEvent $event
     */
    public function handle($event)
    {
        $orderUpup = new OrderUpup(['order_id' => $event->orderId, 'upup_id' => $event->upupOrderId]);
        $this->repository->save($orderUpup);
    }
}