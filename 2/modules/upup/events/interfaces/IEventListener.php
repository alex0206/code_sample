<?php


namespace app\modules\upup\events\interfaces;


interface IEventListener
{
    public function handle($event);
}