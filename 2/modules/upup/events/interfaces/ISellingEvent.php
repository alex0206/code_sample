<?php


namespace app\modules\upup\events\interfaces;


interface ISellingEvent
{
    const ADD_EVENT = 'add_order';
    const UPDATE_EVENT = 'update_order';
    const DELETE_EVENT = 'delete_order';
}