<?php

namespace app\modules\upup;


use app\modules\BaseModule;
use app\modules\upup\components\RequestUrl;
use app\modules\upup\repositories\UpUpSettingsRepository;
use Yii;

/**
 * upup module definition class
 * @property RequestUrl $requestUrl
 */
class Module extends BaseModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\upup\controllers';
    public $protocolVersion;
    private $settings;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->setSettings();

        Yii::configure($this, require(__DIR__ . '/config.php'));
    }

    public function getApiKey(): string
    {
        return $this->settings[UpUpSettingsRepository::API_KEY];
    }

    public function getClid(): string
    {
        return $this->settings[UpUpSettingsRepository::CLID];
    }

    private function setSettings()
    {
        /** @var UpUpSettingsRepository $settingRepository */
        $settingRepository = Yii::createObject(UpUpSettingsRepository::class,
            [$this->getTenantId(), $this->getCityId()]);
        $this->settings = $settingRepository->getSettings();
    }
}
