<?php


namespace app\modules\upup\repositories;


use app\repositories\exceptions\WrongDataException;
use app\repositories\interfaces\IActiveOrders;

class TariffRepository
{
    /**
     * @var IActiveOrders
     */
    private $ordersRepository;
    /**
     * @var int
     */
    private $tenantId;

    public function __construct(int $tenantId, IActiveOrders $ordersRepository)
    {
        $this->ordersRepository = $ordersRepository;
        $this->tenantId = $tenantId;
    }

    /**
     * 'tariff' =>
     *   array (
     *    'tariff_id' => '59',
     *    'tenant_id' => '68',
     *    'class_id' => '1',
     *    'block' => '0',
     *    'group_id' => '7',
     *    'name' => 'Эконом_Ижевск',
     *    'description' => '222',
     *    'sort' => '10',
     *    'auto_downtime' => '1',
     *    'enabled_site' => '1',
     *    'enabled_app' => '1',
     *    'enabled_operator' => '1',
     *    'enabled_bordur' => '0',
     *    'enabled_cabinet' => '1',
     *    'logo' => '56fe9514d7760.jpg',
     *    'position_id' => '1',
     *    'position_class_id' => NULL,
     *    'class' =>
     *       array (
     *         'class_id' => '1',
     *         'class' => 'Econom',
     *    ),
     * )
     * 'costData' =>
     *   array (
     *    'additionals_cost' => NULL,
     *    'summary_time' => '0',
     *    'summary_distance' => '0',
     *    'city_time' => '0',
     *    'city_distance' => '0',
     *    'city_cost' => '0',
     *    'out_city_time' => '0',
     *    'out_city_distance' => '0',
     *    'out_city_cost' => '0',
     *    'is_fix' => 0,
     *    'summary_cost' => '300',
     *    'tariffInfo' =>
     *      array (
     *       'tariffType' => 'CURRENT',
     *       'tariffDataCity' =>
     *         array (
     *           'option_id' => '271',
     *           'tariff_id' => '59',
     *           'accrual' => 'TIME',
     *           'area' => 'CITY',
     *           'planting_price_day' => '70',
     *           'planting_price_night' => '60',
     *           'planting_include_day' => '0',
     *           'planting_include_night' => '1',
     *           'next_km_price_day' => '20',
     *           'next_km_price_night' => '20',
     *           'min_price_day' => '300',
     *           'min_price_night' => '300',
     *           'supply_price_day' => '0',
     *           'supply_price_night' => '0',
     *           'wait_time_day' => '0',
     *           'wait_time_night' => '0',
     *           'wait_driving_time_day' => '10',
     *           'wait_driving_time_night' => '10',
     *           'wait_price_day' => '3.33',
     *           'wait_price_night' => '3.5',
     *           'speed_downtime_day' => '0',
     *           'speed_downtime_night' => '0',
     *           'rounding_day' => '1',
     *           'rounding_night' => '0',
     *           'tariff_type' => 'CURRENT',
     *           'allow_day_night' => '1',
     *           'start_day' => '09:30',
     *           'end_day' => '03:30',
     *           'active' => '1',
     *           'enabled_parking_ratio' => '1',
     *           'calculation_fix' => '0',
     *           'next_cost_unit_day' => '1_minute',
     *           'next_cost_unit_night' => '1_minute',
     *           'next_km_price_day_time' => '230',
     *           'next_km_price_night_time' => '190',
     *           'planting_include_day_time' => '1',
     *           'planting_include_night_time' => '1',
     *        ),
     *       'tariffDataTrack' =>
     *         array (
     *           'option_id' => '272',
     *           'tariff_id' => '59',
     *           'accrual' => 'TIME',
     *           'area' => 'TRACK',
     *           'planting_price_day' => '20',
     *           'planting_price_night' => '65',
     *           'planting_include_day' => '0',
     *           'planting_include_night' => '0',
     *           'next_km_price_day' => '20',
     *           'next_km_price_night' => '30',
     *           'min_price_day' => '300',
     *           'min_price_night' => '300',
     *           'supply_price_day' => '10',
     *           'supply_price_night' => '10',
     *           'wait_time_day' => '0',
     *           'wait_time_night' => '0',
     *           'wait_driving_time_day' => '11',
     *           'wait_driving_time_night' => '11',
     *           'wait_price_day' => '5',
     *           'wait_price_night' => '10',
     *           'speed_downtime_day' => '2',
     *           'speed_downtime_night' => '2',
     *           'rounding_day' => '0',
     *           'rounding_night' => '10',
     *           'tariff_type' => 'CURRENT',
     *           'allow_day_night' => '1',
     *           'start_day' => '09:30',
     *           'end_day' => '03:30',
     *           'active' => '1',
     *           'enabled_parking_ratio' => '1',
     *           'calculation_fix' => '0',
     *           'next_cost_unit_day' => '1_minute',
     *           'next_cost_unit_night' => '1_minute',
     *           'next_km_price_day_time' => '0',
     *           'next_km_price_night_time' => '0',
     *           'planting_include_day_time' => '0',
     *           'planting_include_night_time' => '0',
     *    ),
     *    'isDay' => 1,
     *    ),
     *    'start_point_location' => 'in',
     * )
     * @param int $orderId
     * @return array
     */
    public function getTariff(int $orderId): array
    {
        $orderData = $this->ordersRepository->getOne($this->tenantId, $orderId);

        if (!isset($orderData['tariff'], $orderData['costData'])) {
            throw new WrongDataException('The order data must contain a "tariff" and "costData" array indexes');
        }

        $tariffData = [
            'tariff'   => $orderData['tariff'],
            'costData' => $orderData['costData'],
        ];

        return $tariffData;
    }
}