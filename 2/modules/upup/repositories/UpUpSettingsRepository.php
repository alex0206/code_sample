<?php


namespace app\modules\upup\repositories;


use app\repositories\ExchangeSettingsRepository;

class UpUpSettingsRepository extends ExchangeSettingsRepository
{
    const CLID = 'UPUP_CLID';
    const API_KEY = 'UPUP_API_KEY';

    protected function getSettingNames(): array
    {
        return [
            self::CLID,
            self::API_KEY,
        ];
    }
}