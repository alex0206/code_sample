<?php


namespace app\modules\upup\repositories\order;


use app\modules\upup\models\orders\OrderUpup;
use app\repositories\exceptions\NotFoundException;
use RuntimeException;
use Yii;

class OrderUppRepository
{
    public function getById(int $orderId): OrderUpup
    {
        $res = OrderUpup::findOne(['order_id' => $orderId]);

        if (empty($res)) {
            throw new NotFoundException('The upup order has not been found');
        }

        return $res;
    }

    /**
     * @param OrderUpup $orderUpup
     * @throws RuntimeException
     */
    public function save(OrderUpup $orderUpup)
    {
        if ($orderUpup->save(false) === false) {
            throw new RuntimeException('Error saving of upup model');
        }
    }

    /**
     * @param int $orderId
     * @return int
     */
    public function delete(int $orderId)
    {
        return Yii::$app->db->createCommand()->delete(OrderUpup::tableName(), ['order_id' => $orderId])->execute();
    }
}