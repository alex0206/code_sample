<?php


namespace app\modules\upup\repositories;


use app\modules\upup\models\drivers\Cab;
use app\repositories\exceptions\NotFoundException;
use app\repositories\interfaces\IActiveDriverRepository;
use Yii;

class CabRepository
{
    /**
     * @var IActiveDriverRepository
     */
    private $activeDriverRepository;

    public function __construct(IActiveDriverRepository $activeDriverRepository)
    {
        $this->activeDriverRepository = $activeDriverRepository;
    }

    /**
     * @return Cab[]
     */
    public function getAll(): array
    {
        $drivers = $this->activeDriverRepository->getAll();

        $cabs = [];

        foreach ($drivers as $data) {
            $cab = unserialize($data);

            if (!isset($cab['worker'], $cab['car']) || !$this->isTaxiDriver($cab) || !$this->isExchangeOn($cab)) {
                continue;
            }

            $cab[] = new Cab($cab);
        }

        return $cabs;
    }

    /**
     * @param string $callsign
     * @return Cab
     * @throws NotFoundException
     */
    public function getOne(string $callsign): Cab
    {
        $driverInfo = $this->activeDriverRepository->getOne($callsign);

        if (empty($driverInfo)) {
            throw new NotFoundException('Active driver with callsign: ' . $callsign . ' has not been found');
        }

        return new Cab($driverInfo);
    }

    private function isExchangeOn(array $cabInfo): bool
    {
        return isset($cabInfo['worker']['upup_exchange_on']) && $cabInfo['worker']['upup_exchange_on'] == 1;
    }

    private function isTaxiDriver(array $cabInfo): bool
    {
        return isset($cabInfo['worker']['position']['position_id']) &&
            $cabInfo['worker']['position']['position_id'] == Yii::$app->params['taxi_position_id'];
    }
}