<?php


namespace app\modules\upup\controllers;


use app\controllers\BaseController;
use app\exceptions\BadRequestToService;
use app\modules\upup\events\order\listeners\OrderDeleteListener;
use app\modules\upup\events\order\listeners\OrderSoldListener;
use app\modules\upup\events\order\listeners\OrderUpdateListener;
use app\modules\upup\helpers\RequestUrl;
use app\modules\upup\Module;
use app\modules\upup\services\exceptions\CarAssignException;
use app\modules\upup\services\OrderSellingService;
use app\repositories\exceptions\NotFoundException;
use app\repositories\exceptions\WrongDataException;
use Yii;
use yii\base\Event;
use yii\base\NotSupportedException;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * For selling gootax order to exchange
 * Class OrderSellingController
 * @package app\modules\upup\controllers
 */
class OrderSellingController extends BaseController
{
    use RequestUrl;

    /** @var OrderSellingService */
    private $orderSellingService;

    public function init()
    {
        parent::init();

        $this->orderSellingService = Yii::createObject(OrderSellingService::class, [
            $this->getRequestUrl('api'),
        ]);
    }

    /**
     * POST
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws HttpException
     */
    public function actionAdd()
    {
        $orderId = Yii::$app->request->post('order_id');

        if (empty($orderId)) {
            throw new BadRequestHttpException('Missing order_id');
        }

        try {
            $orderSoldListener = Yii::createObject(OrderSoldListener::class);
            Event::on(OrderSellingService::class, OrderSellingService::ADD_EVENT, [$orderSoldListener, 'handle']);

            $this->orderSellingService->add($orderId);
        } catch (NotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage());
        } catch (WrongDataException | NotSupportedException $e) {
            throw new BadRequestHttpException($e->getMessage());
        } catch (BadRequestToService $e) {
            throw new HttpException(503, $e->getMessage());
        }
    }

    /**
     * PUT
     * @param int $order_id
     * @throws HttpException
     * @throws CarAssignException
     * @return null|array
     */
    public function actionUpdate(int $order_id)
    {
        try {
            $orderUpdateListener = Yii::createObject(OrderUpdateListener::class);
            Event::on(OrderSellingService::class, OrderSellingService::UPDATE_EVENT, [$orderUpdateListener, 'handle']);

            $this->orderSellingService->update($order_id);
        } catch (NotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage());
        } catch (BadRequestToService $e) {
            throw new HttpException(503, $e->getMessage());
        } catch (CarAssignException $e) {
            return ['result' => $e->getMessage()];
        }

        return null;
    }

    /**
     * DELETE
     * @param int $order_id
     * @throws HttpException
     */
    public function actionDelete(int $order_id)
    {
        try {
            $orderDeleteListener = Yii::createObject(OrderDeleteListener::class);
            Event::on(OrderSellingService::class, OrderSellingService::DELETE_EVENT,
                [$orderDeleteListener, 'handle']);

            $this->orderSellingService->delete($order_id);
        } catch (BadRequestToService $e) {
            throw new HttpException(503, $e->getMessage());
        }
    }

    /**
     * GET
     * @param int $order_id
     * @param int $check (0|1) Checking the order without full info
     * @throws HttpException
     * @return array
     */
    public function actionInfo(int $order_id, int $check = 0)
    {
        try {
            return $this->orderSellingService->getInfo($order_id, $check);
        } catch (BadRequestToService $e) {
            throw new HttpException(503, $e->getMessage());
        }
    }

    /**
     * GET
     * @param string $orderid
     * @param string $status
     * @throws NotFoundHttpException
     */
    public function actionChangeStatus($orderid, $status)
    {
        /** @var Module $module */
        $module = $this->module;
        try {
            $this->orderSellingService->changeStatus($orderid, $status, $module->getTenantId());
        } catch (NotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage());
        }
    }
}