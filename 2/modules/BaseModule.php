<?php


namespace app\modules;

use app\filters\interfaces\IFilter;
use app\helpers\TenantRequestDataHelper;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Module;
use yii\web\NotFoundHttpException;

class BaseModule extends Module
{
    /** @var array */
    public $filters = [];

    private $tenantId;
    private $cityId;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->runFilters();
        parent::init();
    }

    public function getTenantId(): int
    {
        if (empty($this->tenantId)) {
            $this->tenantId = TenantRequestDataHelper::getId();
        }

        return $this->tenantId;
    }

    /**
     * @return array|mixed
     * @throws NotFoundHttpException
     */
    public function getCityId(): int
    {
        if (empty($this->cityId)) {
            $this->cityId = Yii::$app->request->get('city_id');
        }

        return $this->cityId;
    }

    private function runFilters()
    {
        if (!empty($this->filters)) {
            foreach ($this->filters as $filter) {
                /** @var IFilter $filter */
                $filter = Yii::createObject($filter);
                if (!$filter instanceof IFilter) {
                    throw new InvalidConfigException('The filter must be implement of IFilter');
                }

                $filter->run();
            }
        }
    }
}