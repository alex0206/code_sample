<?php


namespace console\modules\statistic\models\order\interfaces;


interface IOrderRepository
{
    public function getById($orderId);

    public function getStatusLog($orderId);

    public function getSummaryCost($orderId);

    public function getBadFeedbackRating($orderId);
}