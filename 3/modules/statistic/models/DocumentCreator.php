<?php


namespace console\modules\statistic\models;


use console\modules\statistic\models\helpers\DeviceHelper;

abstract class DocumentCreator
{
    /**
     * @var array
     */
    protected $orderData;

    public function __construct(array $orderData)
    {
        $this->orderData = $orderData;
    }

    abstract public function create();

    /**
     * @return array
     */
    protected function getDevices()
    {
        return [
            DeviceHelper::IOS        => [],
            DeviceHelper::ANDROID    => [],
            DeviceHelper::DISPATCHER => 0,
            DeviceHelper::WORKER     => 0,
            DeviceHelper::WEB        => 0,
            DeviceHelper::YANDEX     => 0,
        ];
    }
}