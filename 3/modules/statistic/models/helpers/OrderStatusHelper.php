<?php


namespace console\modules\statistic\models\helpers;


class OrderStatusHelper
{
    const CAR_AT_PLACE = 26;
    const PRE_ORDER = 6;
    const PRE_ORDER_CAR_ASSIGNED = 7;
    const LATED_DRIVER = 54;

    /**
     * @return array
     */
    public static function getWarningStatuses()
    {
        return [5, 10, 16, 38, 52, 54, 102];
    }
}