<?php


namespace console\modules\statistic\models\helpers;


class DeviceHelper
{
    const IOS = 'IOS';
    const ANDROID = 'ANDROID';
    const DISPATCHER = 'DISPATCHER';
    const WORKER = 'WORKER';
    const WEB = 'WEB';
    const YANDEX = 'YANDEX';

    /**
     * @param $device
     * @return bool
     */
    public static function isMobile($device)
    {
        return in_array($device, self::getMobiles());
    }

    /**
     * @return array
     */
    public static function getMobiles()
    {
        return [
            self::IOS,
            self::ANDROID,
        ];
    }
}