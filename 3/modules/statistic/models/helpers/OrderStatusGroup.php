<?php


namespace console\modules\statistic\models\helpers;


class OrderStatusGroup
{
    const NEW_GROUP = 'new';
    const COMPLETED = 'completed';
    const REJECTED = 'rejected';
    const PRE_ORDER = 'pre_order';
    const EXECUTING = 'executing';
    const CAR_ASSIGNED = 'car_assigned';

    /**
     * @param string $statusGroup
     * @return bool
     */
    public static function isFinished($statusGroup)
    {
        return $statusGroup == self::COMPLETED || $statusGroup == self::REJECTED;
    }

    /**
     * @param string $statusGroup
     * @return bool
     */
    public static function isPreOrder($statusGroup)
    {
        return $statusGroup == self::PRE_ORDER;
    }

    /**
     * @param string $statusGroup
     * @return bool
     */
    public static function isCompleted($statusGroup)
    {
        return $statusGroup == self::COMPLETED;
    }

    /**
     * @param string $statusGroup
     * @return bool
     */
    public static function isRejected($statusGroup)
    {
        return $statusGroup == self::REJECTED;
    }

    /**
     * @param $statusGroup
     * @return bool
     */
    public static function isReceived($statusGroup)
    {
        return in_array($statusGroup, self::getReceived());
    }

    /**
     * @return array
     */
    public static function getReceived()
    {
        return [
            self::NEW_GROUP,
            self::PRE_ORDER,
            self::EXECUTING,
            self::CAR_ASSIGNED,
        ];
    }
}