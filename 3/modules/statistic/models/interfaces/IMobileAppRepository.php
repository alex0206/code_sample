<?php


namespace console\modules\statistic\models\interfaces;


interface IMobileAppRepository
{
    public function getAppId($tenantId);
}