<?php


namespace console\modules\statistic\models\worker;


use console\modules\statistic\models\DocumentCreator;

class WorkerDocumentCreator extends DocumentCreator
{
    /**
     * @return WorkerStatistic
     */
    public function create()
    {
        return new WorkerStatistic([
            'tenant_id'   => $this->orderData['tenant_id'],
            'worker_id'   => $this->orderData['worker_id'],
            'currency_id' => $this->orderData['currency_id'],
            'position_id' => $this->orderData['position_id'],
            'statistics'  => $this->getStatistic(),
            'date'        => $this->getDate(),
            'timestamp'   => $this->getTimestamp(),
        ]);
    }

    /**
     * @return false|string
     */
    private function getDate()
    {
        return date('d.m.Y', $this->orderData['create_time']);
    }

    /**
     * @return false|int
     */
    private function getTimestamp()
    {
        return mktime(23, 59, 59, date("n", $this->orderData['create_time']),
            date("j", $this->orderData['create_time']),
            date("Y", $this->orderData['create_time']));
    }

    /**
     * @return array
     */
    private function getStatistic()
    {
        return [
            'received'     => [
                'quantity' => 0,
                'device'   => $this->getDevices(),
                'detail'   => [],
            ],
            'pre_order'    => [
                'quantity' => 0,
                'device'   => $this->getDevices(),
                'detail'   => [],
            ],
            'completed'    => [
                'quantity' => 0,
                'sum'      => 0,
                'device'   => $this->getDevices(),
                'detail'   => [
                    'tariffs'  => [],
                    'payment'  => [],
                    'averages' => [
                        'price'            => 0,
                        'pick_up'          => 0,
                        'pick_up_sum_time' => 0,
                    ],
                ],
            ],
            'rejected'     => [
                'quantity' => 0,
                'sum'      => 0,
                'device'   => $this->getDevices(),
                'detail'   => [
                    'reasons' => [
                        'rejected' => [],
                        'warning'  => [],
                    ],
                ],
            ],
            'bad_feedback' => [
                'one' => 0,
                'two' => 0,
            ],
        ];
    }
}