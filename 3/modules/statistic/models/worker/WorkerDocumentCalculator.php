<?php


namespace console\modules\statistic\models\worker;


use console\modules\statistic\models\helpers\OrderStatusHelper;

class WorkerDocumentCalculator
{
    /**
     * @var WorkerStatistic
     */
    private $document;
    /**
     * @var array
     */
    private $orderData;

    public function __construct(WorkerStatistic $document, array $orderData)
    {
        $this->document = $document;
        $this->orderData = $orderData;
    }

    public function calculate()
    {
        $statistics = $this->document->statistics;

        //Принято
        //Количество
        $statistics['received']['quantity']++;
        //Устройство
        if (!empty($this->orderData['device'])) {
            $statistics['received']['device'][$this->orderData['device']]++;
        }

        if (!empty($this->orderData['parking_id'])) {
            //Детально - количество
            $statistics['received']['detail'][$this->orderData['parking_id']]['quantity']++;
            //Детально - устройство
            if (!empty($this->orderData['device'])) {
                $statistics['received']['detail'][$this->orderData['parking_id']][$this->orderData['device']]++;
            }
        }

        //Предварительный заказ
        if (isset($this->orderData['status_log']) && in_array(OrderStatusHelper::PRE_ORDER_CAR_ASSIGNED,
                $this->orderData['status_log'])
        ) {
            //Количество
            $statistics['pre_order']['quantity']++;
            //Устройство
            if (!empty($this->orderData['device'])) {
                $statistics['pre_order']['device'][$this->orderData['device']]++;
            }

            if (!empty($this->orderData['parking_id'])) {
                //Детально - количество
                $statistics['pre_order']['detail'][$this->orderData['parking_id']]['quantity']++;
                //Детально - устройство
                if (!empty($this->orderData['device'])) {
                    $statistics['pre_order']['detail'][$this->orderData['parking_id']][$this->orderData['device']]++;
                }
            }
        }

        //Выполнено
        if ($this->orderData['status_group'] == 'completed') {
            //Количество
            $statistics['completed']['quantity']++;
            //Сумма
            $statistics['completed']['sum'] += $this->orderData['price'];
            //Устройство
            if (!empty($this->orderData['device'])) {
                $statistics['completed']['device'][$this->orderData['device']]++;
            }
            //Детально - Тарифы
            //Количество
            if (!empty($this->orderData['tariff_id'])) {
                $statistics['completed']['detail']['tariffs'][$this->orderData['tariff_id']]['quantity']++;
                //Cумма
                $statistics['completed']['detail']['tariffs'][$this->orderData['tariff_id']]['sum'] += $this->orderData['price'];
                //Устройство
                if (!empty($this->orderData['device'])) {
                    $statistics['completed']['detail']['tariffs'][$this->orderData['tariff_id']][$this->orderData['device']]++;
                }
            }
            //Детально - Виды оплат
            if (!empty($this->orderData['payment'])) {
                //Количество
                $statistics['completed']['detail']['payment'][$this->orderData['payment']]['quantity']++;
                //Cумма
                $statistics['completed']['detail']['payment'][$this->orderData['payment']]['sum'] += $this->orderData['price'];
                //Устройство
                if (!empty($this->orderData['device'])) {
                    $statistics['completed']['detail']['payment'][$this->orderData['payment']][$this->orderData['device']]++;
                }
            }
            //Детально - Средние показатели
            $statistics['completed']['detail']['averages']['price'] = round($statistics['completed']['sum'] / $statistics['completed']['quantity'],
                2);
            $statistics['completed']['detail']['averages']['pick_up_sum_time'] += $this->orderData['pick_up'];
            $statistics['completed']['detail']['averages']['pick_up'] += round($statistics['completed']['detail']['averages']['pick_up_sum_time'] / $statistics['completed']['quantity']);
        } //Отмененные
        elseif ($this->orderData['status_group'] == 'rejected') {
            //Количество
            $statistics['rejected']['quantity']++;
            //Cумма
            $statistics['rejected']['sum'] += $this->orderData['predv_price'] != 0 ? $this->orderData['predv_price'] : $statistics['completed']['detail']['averages']['price'];
            //Устройство
            if (!empty($this->orderData['device'])) {
                $statistics['rejected']['device'][$this->orderData['device']]++;
            }
            //Детально - Список причин отмены
            if (!empty($this->orderData['status_id'])) {
                $statistics['rejected']['detail']['reasons']['rejected'][$this->orderData['status_id']]++;
            }

            if (!empty($this->orderData['warning'])) {
                foreach ($this->orderData['warning'] as $warning) {
                    if (!empty($warning)) {
                        $statistics['rejected']['detail']['reasons']['warning'][$warning]['rejected']++;
                    }
                }
            }
        }

        if (!empty($this->orderData['warning'])) {
            foreach ($this->orderData['warning'] as $warning) {
                if (!empty($warning)) {
                    $statistics['rejected']['detail']['reasons']['warning'][$warning]['cnt']++;
                }
            }
        }

        $this->document->statistics = $statistics;
    }
}