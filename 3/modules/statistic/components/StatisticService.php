<?php


namespace console\modules\statistic\components;


use console\modules\statistic\events\StatisticSaveEvent;
use console\modules\statistic\models\helpers\OrderStatusGroup;
use console\modules\statistic\models\interfaces\IStatisticService;
use console\modules\statistic\models\worker\WorkerStatisticService;
use console\modules\statistic\models\order\DocumentService;
use console\modules\statistic\repositories\OrderRepository;
use Yii;

class StatisticService implements IStatisticService
{
    /**
     * @var int
     */
    private $orderId;
    private $documentService;

    public function __construct($orderId)
    {
        $this->orderId = $orderId;
        $this->documentService = $this->getDocumentComponent();
    }

    /**
     * @return bool
     */
    public function addOrder()
    {
        $this->documentService->on(DocumentService::EVENT_AFTER_SAVE, function (StatisticSaveEvent $event) {
            $orderData = $event->orderData;
            if (OrderStatusGroup::isFinished($orderData['status_group']) && !empty($orderData['worker_id'])) {
                $this->getWorkerStatisticService($orderData)->addOrder();
            }
        });

        return $this->documentService->addOrder();
    }

    /**
     * @return bool
     */
    public function addFeedback()
    {
        $this->documentService->on(DocumentService::EVENT_AFTER_SAVE, function (StatisticSaveEvent $event) {
            $this->getWorkerStatisticService($event->orderData)->addFeedback();
        });

        return $this->documentService->addFeedback();
    }

    /**
     * @return DocumentService
     */
    private function getDocumentComponent()
    {
        /** @var OrderRepository $orderRepository */
        $orderRepository = Yii::createObject(OrderRepository::class);
        /** @var DocumentService $documentComponet */
        $documentComponet = Yii::createObject(DocumentService::className(), [$this->orderId, $orderRepository]);

        return $documentComponet;
    }

    /**
     * @param array $orderData
     * @return WorkerStatisticService
     */
    private function getWorkerStatisticService(array $orderData)
    {
        /** @var WorkerStatisticService $workerStatisticService */
        $workerStatisticService = Yii::createObject(WorkerStatisticService::class, [$orderData]);

        return $workerStatisticService;
    }
}