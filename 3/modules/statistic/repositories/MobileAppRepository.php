<?php


namespace console\modules\statistic\repositories;


use console\modules\statistic\models\interfaces\IMobileAppRepository;
use yii\db\Query;

class MobileAppRepository implements IMobileAppRepository
{
    public function getAppId($tenantId)
    {
        return (new Query())
            ->select('app_id')
            ->from('{{%mobile_app}}')
            ->where(['tenant_id' => $tenantId])
            ->scalar();
    }
}