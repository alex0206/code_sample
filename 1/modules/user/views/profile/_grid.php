<?php

use backend\modules\user\models\Profile;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $pjaxId string */
/* @var $emptyText string */

$emptyText = $emptyText ?? null;
?>

<div class="input-row">
    <?php
    Pjax::begin(['id' => $pjaxId]);
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'       => "{items}\n{pager}",
        'tableOptions' => [
            'class' => 'border-header',
        ],
        'columns'      => [
            [
                'attribute' => 'name',
                'content'   => function (Profile $model) {
                    return Html::a($model->name, ['/user/profile/update', 'id' => $model->user_id],
                        ['data-pjax' => 0]);
                },
            ],
            'email:email',
            'phone',
            'position.name',
        ],
        'emptyText'    => $emptyText,
        'showOnEmpty'  => false,
    ]);
    Pjax::end();
    ?>
</div>
