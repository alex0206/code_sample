<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model \backend\modules\user\models\InviteForm */
/* @var $message string */

Pjax::begin(['id' => 'pjax-invite-user-form', 'enablePushState' => false]);

$form = ActiveForm::begin([
    'action'      => ['/user/recruit/invite'],
    'options'     => ['data-pjax' => true],
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"input-row-content\">{input}</div>\n{hint}\n{error}",
        'options'  => ['class' => 'input-row'],
    ],
]); ?>
<?php if ($message):
    $this->registerJs('$.pjax.reload({container:"#pjax-invited-users", url: "' . Url::to('/user/recruit/invited-list') .
        '", push: false, replace: false}); $("#user_invited").show();');
    ?>
    <p class="green-text"><?= $message ?></p>
<?php endif; ?>
<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'position_id')->dropDownList($model->getPositionMap()) ?>
<div class="input-row">
    <label></label>
    <div class="input-row-content">
        <?= Html::submitButton('Отправить', ['class' => 'button']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<?php Pjax::end() ?>
