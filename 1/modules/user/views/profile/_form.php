<?php

use backend\modules\user\assets\profile\ProfileAsset;
use backend\modules\user\models\Profile;
use common\models\user\Position;
use common\modules\photo\widget\logo\Logo;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model Profile */
/* @var $form yii\widgets\ActiveForm */
/* @var $logo \common\modules\photo\models\Photo */

$this->registerAssetBundle(ProfileAsset::className());
?>

<section class="island">

    <?php $form = ActiveForm::begin([
        'options'     => ['enctype' => 'multipart/form-data'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"input-row-content\">{input}</div>\n{hint}\n{error}",
            'options'  => ['class' => 'input-row'],
        ],
    ]); ?>

    <?= $form->field($logo, 'small')
        ->label('')
        ->widget(Logo::className()); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php if ($model->isStrangerScenario()): ?>
        <?= $form->field($model, 'position_id')->dropDownList(Position::getMap()) ?>
    <?php endif; ?>

    <?= $form->field($model, 'birthday')->textInput(['class' => 'js-datepicker']) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <div class="input-row">
        <label><?= $model->getAttributeLabel('phone') ?></label>
        <div class="input-row-content">
            <div class="input-phone">
                <?php $cssHintClass = $model->phone_confirmed ? 'small green-text' : 'small red-text' ?>
                <?= $form->field($model, 'phone', [
                    'options'  => ['class' => 'input-phone__phone'],
                    'template' => "{input}\n{hint}\n{error}",
                ])
                    ->widget(MaskedInput::className(), ['mask' => '+9 (999) 999 99 99',])
                    ->hint($model->phone_confirmed ? 'Телефон подтверждён по SMS' : 'Требуется подтверждение',
                        ['tag' => 'div', 'class' => $cssHintClass])
                ?>
                <div class="input-phone__social">
                    <?= $form->field($model, 'whatsapp', [
                        'options'  => ['class' => 'input-phone__social__item'],
                        'template' => "<i class=\"social-icon social-icon__wahtsapp\"></i>{label}\n{input}",
                    ])->checkbox() ?>

                    <?= $form->field($model, 'viber', [
                        'options'  => ['class' => 'input-phone__social__item'],
                        'template' => "<i class=\"social-icon social-icon__viber\"></i>{label}\n{input}",
                    ])->checkbox() ?>

                    <?= $form->field($model, 'telegram', [
                        'options'  => ['class' => 'input-phone__social__item'],
                        'template' => "<i class=\"social-icon social-icon__telegram\"></i>{label}\n{input}",
                    ])->checkbox() ?>
                </div>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'phone_confirm_code', [
        'options'  => [
            'style' => !$model->phone_confirmed ?: 'display: none',
        ],
        'template' => "{label}\n<div class=\"input-row-content\"><div class=\"input-phone__phone\">{input}</div>" .
            "<div class=\"input-phone__confirm\"><i class=\"ok\" style=\"display: none\"></i><a>Отправить <span></span></a></div>" .
            "</div>\n{hint}\n{error}",
    ])->textInput(['maxlength' => 4, 'placeholder' => 'Код подтверждения']) ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'password_repeat')->passwordInput() ?>

    <?= $form->field($model, 'about_me')->textarea(['rows' => 3]) ?>

    <div class="input-row">
        <label>Доп. каналы для общения</label>
        <div class="input-row-content">
            <div class="social-accounts">
                <?= $form->field($model, 'skype', [
                    'options'  => ['class' => 'social-accounts__item'],
                    'template' => "{input}\n<i class=\"social-icon social-icon__skype\"></i>\n{error}",
                ])->textInput(['placeholder' => $model->getAttributeLabel('skype'), 'maxlength' => true]) ?>

                <?= $form->field($model, 'vk', [
                    'options'  => ['class' => 'social-accounts__item'],
                    'template' => "{input}\n<i class=\"social-icon social-icon__vk\"></i>\n{error}",
                ])->textInput(['placeholder' => $model->getAttributeLabel('vk'), 'maxlength' => true]) ?>

                <?= $form->field($model, 'facebook', [
                    'options'  => ['class' => 'social-accounts__item'],
                    'template' => "{input}\n<i class=\"social-icon social-icon__facebook\"></i>\n{error}",
                ])->textInput(['placeholder' => $model->getAttributeLabel('facebook'), 'maxlength' => true]) ?>
            </div>
        </div>
    </div>

    <div class="input-row">
        <label></label>
        <div class="input-row-content">
            <?= Html::submitButton('Сохранить', ['class' => 'button']) ?>
            <?php if ($model->isStrangerScenario()): ?>
                <?= Html::a('Уволить', ['/user/profile/remove', 'id' => $model->user_id],
                    ['class' => 'button red-button delete-profile']) ?>
            <?php endif; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</section>
