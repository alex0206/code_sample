<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \common\models\user\User */

$this->title = $model->name;
?>
<section class="island">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="input-row">
        <?= DetailView::widget([
            'template'   => '<tr><td{captionOptions}>{label}</td><td{contentOptions}>{value}</td></tr>',
            'model'      => $model,
            'attributes' => [
                'name',
                'birthday',
                'email:email',
                'position.name',
                'phone',
                'about_me:ntext',
                'skype',
                'facebook',
                'vk',
                'telegram:boolean',
                'whatsapp:boolean',
                'viber:boolean',
            ],
        ]) ?>
    </div>
</section>
