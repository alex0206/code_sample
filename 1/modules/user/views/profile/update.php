<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \backend\modules\user\models\Profile */
/* @var $logo \common\modules\photo\models\Photo */

$this->title = $model->name;
?>
<h1 class="with-go-back">
    <?= Html::a('', ['index'], ['class' => 'icon icon__go-back']) ?>
    <?= Html::encode($model->name) ?>
</h1>

<?= $this->render('_form', [
    'model' => $model,
    'logo'  => $logo,
]) ?>

