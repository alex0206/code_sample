<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сотрудники';
?>
<h1><?= Html::encode($this->title) ?></h1>
<section class="island">
    <div class="tabs">
        <div class="tabs__header">
            <div class="tabs__header__item tabs-active" data-tab-id="t01">
                В штате
            </div>
            <div class="tabs__header__item" data-tab-id="t02" data-href="<?= Url::to(['/user/profile/recruits']) ?>">
                Новые
            </div>
            <div class="tabs__header__item" data-tab-id="t03" data-href="<?= Url::to(['/user/profile/fired']) ?>">
                Уволенные
            </div>
        </div>
        <div class="tabs__content">
            <div class="tabs__content__item tabs-active" data-tab-id="t01">
                <?= $this->render('_grid', ['dataProvider' => $dataProvider, 'pjaxId' => 'pjax-working-users']) ?>
            </div>
            <div class="tabs__content__item" data-tab-id="t02"></div>
            <div class="tabs__content__item" data-tab-id="t03"></div>
        </div>
    </div>
</section>
