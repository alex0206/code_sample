<?php

/* @var $this yii\web\View */
/* @var $newDataProvider \yii\data\ActiveDataProvider */
/* @var $invitedDataProvider \yii\data\ActiveDataProvider */
/* @var $inviteForm \backend\modules\user\models\InviteForm */

$this->registerAssetBundle(\backend\modules\user\assets\inviteButtons\InviteButtonsAsset::className());
?>
<h2>Выслать приглашение новому сотруднику</h2>
<?= $this->render('_inviteForm', ['model' => $inviteForm, 'message' => null]) ?>

<div id="new_user_requests"<?php if (!$newDataProvider->getCount()): ?> style="display: none"<?php endif; ?>>
    <h2>Новые заявки</h2>
    <?= $this->render('/recruit/_newRequestGrid', ['dataProvider' => $newDataProvider]) ?>
</div>
<div id="user_invited"<?php if (!$invitedDataProvider->getCount()): ?> style="display: none"<?php endif; ?>>
    <h2>Приглашённые</h2>
    <?= $this->render('/recruit/_invitedGrid', ['dataProvider' => $invitedDataProvider]) ?>
</div>