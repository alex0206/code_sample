<?php

use backend\modules\user\models\Profile;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="input-row">
    <?php
    Pjax::begin(['id' => 'pjax-invited-users', 'enablePushState' => false]);
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'       => "{items}\n{pager}",
        'tableOptions' => [
            'class' => 'border-header',
        ],
        'columns'      => [
            [
                'attribute' => 'name',
                'content'   => function (Profile $model) {
                    return Html::a($model->name, ['/user/profile/update', 'id' => $model->user_id],
                        ['data-pjax' => 0]);
                },
            ],
            'email:email',
            'position.name',
            [
                'class'      => 'yii\grid\ActionColumn',
                'header'     => 'Действия',
                'controller' => 'invite-buttons',
                'template'   => '{send-again}<br>{remove}',
                'buttons'    => [
                    'send-again' => function ($url, $model, $key) {
                        return Html::a('Выслать повторно <span></span>', $url,
                            ['data-pjax' => 0, 'class' => 'js-invite-again']);
                    },
                    'remove'     => function ($url, $model, $key) {
                        return Html::a('Отменить', $url, ['class' => 'red js-invite-remove', 'data-pjax' => 0]);
                    },
                ],
            ],
        ],
    ]);
    Pjax::end();
    ?>
</div>
