<?php

use backend\modules\user\models\Profile;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<div class="input-row">
    <?php
    Pjax::begin(['id' => 'pjax-new-request-users', 'enablePushState' => false]);
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'       => "{items}\n{pager}",
        'tableOptions' => [
            'class' => 'border-header',
        ],
        'columns'      => [
            [
                'attribute' => 'name',
                'content'   => function (Profile $model) {
                    return Html::a($model->name, ['/user/profile/update', 'id' => $model->user_id],
                        ['data-pjax' => 0]);
                },
            ],
            'email:email',
            'position.name',
            [
                'class'      => 'yii\grid\ActionColumn',
                'header'     => 'Действия',
                'controller' => 'invite-buttons',
                'template'   => '{confirm}<br>{remove}',
                'buttons'    => [
                    'confirm' => function ($url, $model, $key) {
                        return Html::a('Подтвердить', $url, ['class' => 'js-invite-confirm']);
                    },
                    'remove'  => function ($url, $model, $key) {
                        return Html::a('Отклонить', $url, ['class' => 'red js-invite-remove']);
                    },
                ],
            ],
        ],
    ]);
    Pjax::end();
    ?>
</div>
