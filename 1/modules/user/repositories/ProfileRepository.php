<?php


namespace backend\modules\user\repositories;

use common\exceptions\NotFoundException;
use common\models\user\User;
use RuntimeException;
use backend\modules\user\models\Profile;

class ProfileRepository
{
    /**
     * @param int $id
     * @return Profile
     * @throws NotFoundException
     */
    public function findOne(int $id)
    {
        $profile = Profile::findOne($id);

        if (!$profile) {
            throw new NotFoundException('User is not found with id: ' . $id);
        }

        return $profile;
    }

    /**
     * @param Profile $model
     * @throws RuntimeException
     */
    public function save(Profile $model)
    {
        if ($model->save(false) === false) {
            throw new RuntimeException('Error saving the user profile.');
        }
    }

    /**
     * @param User $model
     * @throws RuntimeException
     */
    public function delete(User $model)
    {
        if ($model->delete() === false) {
            throw new RuntimeException('Error deleting the user model.');
        }
    }
}