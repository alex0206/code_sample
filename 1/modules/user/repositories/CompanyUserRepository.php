<?php


namespace backend\modules\user\repositories;


use common\exceptions\NotFoundException;
use common\models\company\CompanyHasUser;
use RuntimeException;

class CompanyUserRepository
{
    /**
     * @param int $userId
     * @param int $companyId
     * @return CompanyHasUser
     * @throws NotFoundException
     */
    public function findOne(int $userId, int $companyId)
    {
        $companyHasUser = CompanyHasUser::findOne(['user_id' => $userId, 'company_id' => $companyId]);

        if (!$companyHasUser) {
            throw new NotFoundException("Company: $companyId is not found of user: $userId");
        }

        return $companyHasUser;
    }

    /**
     * @param CompanyHasUser $model
     * @throws RuntimeException
     */
    public function save(CompanyHasUser $model)
    {
        if ($model->save(false) === false) {
            throw new RuntimeException('Error saving the company user model.');
        }
    }

    /**
     * @param CompanyHasUser $model
     * @throws RuntimeException
     */
    public function delete(CompanyHasUser $model)
    {
        if ($model->delete() === false) {
            throw new RuntimeException('Error deleting the company user model.');
        }
    }
}