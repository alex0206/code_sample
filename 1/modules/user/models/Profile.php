<?php


namespace backend\modules\user\models;


use backend\modules\user\helpers\SecurityHelper;
use backend\modules\user\validators\ConfirmSmsCodeValidator;
use common\helpers\PhoneHelper;
use common\models\user\User;
use yii\helpers\ArrayHelper;

class Profile extends User
{
    const SCENARIO_STRANGER = 'stranger_profile';

    public $password;
    public $password_repeat;
    public $phone_confirm_code;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'birthday', 'name', 'email'], 'required'],
            [['position_id'], 'required', 'on' => self::SCENARIO_STRANGER],
            [
                'birthday',
                'filter',
                'filter' => function ($value) {
                    return date('Y-m-d', strtotime($value));
                },
            ],
            [['birthday'], 'date', 'format' => 'Y-m-d'],
            [['telegram', 'whatsapp', 'viber'], 'boolean'],
            [
                ['phone'],
                'filter',
                'filter' => function ($value) {
                    return PhoneHelper::filter($value);
                },
            ],
            [['phone', 'email'], 'unique'],
            [['about_me'], 'string'],
            ['password', 'string', 'min' => 6],
            [
                ['name', 'email', 'skype', 'facebook', 'vk'],
                'string',
                'max' => 255,
            ],
            [['phone'], 'string', 'max' => 45],
            [
                'password_repeat',
                'compare',
                'compareAttribute' => 'password',
                'message'          => 'Пароли не совпадают',
            ],
            [
                'password_repeat',
                'required',
                'when'       => function ($model) {
                    return !empty($model->password);
                },
                'whenClient' => "function(attr, value){
                    var password = $('#{$this->getFormName()}-password').val();
                    return password != '';
                }",
            ],
            ['email', 'email'],
            [
                'phone_confirm_code',
                'required',
                'when'       => function (Profile $model) {
                    return !$model->phone_confirmed || $model->getOldAttribute('phone') !== $model->phone;
                },
                'whenClient' => "function(attr, value){
                    var phone = $('#{$this->getFormName()}-phone').inputmask('unmaskedvalue');
                    return phone != '{$this->getOldAttribute('phone')}';
                }",
            ],
            [['phone_confirm_code'], ConfirmSmsCodeValidator::className()],
            [['phone_confirm_code'], 'string', 'min' => 4],
        ];
    }

    public function afterValidate()
    {
        parent::afterValidate();
        if (!$this->phone_confirmed && !$this->hasErrors('phone') && !$this->hasErrors('phone_confirm_code')) {
            $this->phone_confirmed = 1;
        }
    }

    /**
     * @return string
     */
    private function getFormName()
    {
        return strtolower($this->formName());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'password'           => 'Изменить пароль',
            'password_repeat'    => 'Повторите новый пароль',
            'position.name'      => 'Должность',
            'phone_confirm_code' => 'Подтвердить по смс',
        ]);
    }

    /**
     * @param int $length
     */
    public function generatePassword(int $length = 10)
    {
        $this->password = SecurityHelper::passwordGenerate($length);
        $this->setPassword($this->password);
    }

    public function isStrangerScenario()
    {
        return $this->scenario === self::SCENARIO_STRANGER;
    }
}