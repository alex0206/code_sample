<?php


namespace backend\modules\user\models;


use common\models\user\Position;
use yii\base\Model;

class InviteForm extends Model
{
    public $email;
    public $name;
    public $position_id;

    private $positionMap;

    public function rules()
    {
        return [
            [['email', 'name', 'position_id'], 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => Profile::className()],
            ['position_id', 'in', 'range' => $this->getPositionKeyList()],
        ];
    }

    public function getPositionMap()
    {
        if (empty($this->positionMap)) {
            $this->positionMap = Position::getMap();
        }

        return $this->positionMap;
    }

    public function attributeLabels()
    {
        return [
            'email'       => 'Эл.почта',
            'name'        => 'ФИО',
            'position_id' => 'Должность',
        ];
    }

    private function getPositionKeyList()
    {
        return array_keys($this->getPositionMap());
    }

    public function clearFields()
    {
        $fields = $this->getAttributes();

        foreach ($fields as $name => $value) {
            $this->$name = null;
        }
    }
}