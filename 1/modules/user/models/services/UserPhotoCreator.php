<?php


namespace backend\modules\user\models\services;


use common\modules\photo\models\PhotoCategory;
use common\modules\photo\models\PhotoCreator;
use common\modules\photo\models\PhotoType;
use common\modules\photo\models\UploadPhotoInterface;
use Yii;

class UserPhotoCreator
{
    public static function create(int $objectId): UploadPhotoInterface
    {
        /** @var PhotoCreator $photoCreator */
        $photoCreator = Yii::createObject('common\modules\photo\models\PhotoCreator',
            [PhotoType::USER, $objectId, PhotoCategory::LOGO]);

        return $photoCreator->create();
    }
}