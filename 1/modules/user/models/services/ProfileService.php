<?php

namespace backend\modules\user\models\services;


use backend\modules\user\models\Profile;
use backend\modules\user\repositories\CompanyUserRepository;
use backend\modules\user\repositories\ProfileRepository;
use common\models\company\CompanyHasUser;
use common\models\user\User;
use common\modules\photo\models\Photo;
use common\modules\photo\models\PhotoType;
use common\modules\photo\models\UploadPhotoInterface;
use DomainException;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\web\IdentityInterface;

class ProfileService
{
    private $repository;
    private $userCompanyRepository;
    /** @var User */
    private $currentUser;

    public function __construct(
        ProfileRepository $repository,
        CompanyUserRepository $userCompanyRepository,
        IdentityInterface $currentUser
    ) {
        $this->repository = $repository;
        $this->userCompanyRepository = $userCompanyRepository;
        $this->currentUser = $currentUser;
    }

    /**
     * @param Profile $profile
     */
    public function update(Profile $profile)
    {
        if (!empty($profile->password)) {
            $profile->setPassword($profile->password);
        }

        $this->repository->save($profile);
    }

    /**
     * @param $status
     * @param array|Sort|boolean $sort the sort definition to be used by this data provider.
     * This can be one of the following:
     *
     * - a configuration array for creating the sort definition object. The "class" element defaults
     *   to 'yii\data\Sort'
     * - an instance of [[Sort]] or its subclass
     * - false, if sorting needs to be disabled.
     *
     * @return ActiveDataProvider
     */
    public function getDataProvider(string $status, $sort = false)
    {
        return new ActiveDataProvider([
            'query' => Profile::find()->with('position')->getByStatus($status),
            'sort'  => $sort,
        ]);
    }

    public function getRepository()
    {
        return $this->repository;
    }

    public function getModel(int $id): Profile
    {
        $model = $this->repository->findOne($id);

        if ($model->user_id !== $this->currentUser->user_id) {
            $model->scenario = Profile::SCENARIO_STRANGER;
        }

        return $model;
    }

    public function getPictureModel(Profile $model): UploadPhotoInterface
    {
        /** @var Photo $photo */
        $photo = $model->photo;

        if (!$photo) {
            return UserPhotoCreator::create($model->user_id);
        }

        return $photo;
    }

    /**
     * @param int $userId
     */
    public function fire(int $userId)
    {
        $companyHasUser = $this->getCompanyHasUser($userId);

        if ($companyHasUser->status === CompanyHasUser::FIRED_STATUS) {
            throw new DomainException('The employee have already fired');
        }

        $companyHasUser->status = CompanyHasUser::FIRED_STATUS;
        $this->userCompanyRepository->save($companyHasUser);
    }

    /**
     * @param int $userId
     * @return CompanyHasUser
     */
    private function getCompanyHasUser(int $userId)
    {
        $companyId = $this->currentUser->getWorkCompanyId();

        return $this->userCompanyRepository->findOne($userId, $companyId);
    }
}