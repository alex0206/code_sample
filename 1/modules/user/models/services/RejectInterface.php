<?php


namespace backend\modules\user\models\services;


use common\models\company\CompanyHasUser;

interface RejectInterface
{
    public function execute(CompanyHasUser $companyHasUser);
}