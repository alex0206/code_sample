<?php


namespace backend\modules\user\models\services;


use backend\modules\user\models\InviteForm;
use backend\modules\user\repositories\ProfileRepository;
use backend\modules\user\repositories\CompanyUserRepository;
use common\components\TransactionManager;
use common\exceptions\NotFoundException;
use common\models\company\CompanyHasUser;
use common\models\user\User;
use common\modules\auth\models\EmailConfimer;
use yii\web\IdentityInterface;

class InviteService
{
    private $profileRepository;
    private $userCompanyRepository;
    private $transactionManager;
    private $mailSender;
    private $userCreator;
    private $companyUserCreator;
    private $inviteConfimer;
    /** @var User */
    private $currentUser;

    public function __construct(
        ProfileRepository $profileRepository,
        CompanyUserRepository $userCompanyRepository,
        TransactionManager $transactionManager,
        InviteMailNotifier $mailSender,
        InviteUserFactory $userCreator,
        CompanyUserFactory $companyUserCreator,
        EmailConfimer $inviteConfimer,
        IdentityInterface $currentUser
    ) {
        $this->profileRepository = $profileRepository;
        $this->userCompanyRepository = $userCompanyRepository;
        $this->transactionManager = $transactionManager;
        $this->mailSender = $mailSender;
        $this->userCreator = $userCreator;
        $this->companyUserCreator = $companyUserCreator;
        $this->inviteConfimer = $inviteConfimer;
        $this->currentUser = $currentUser;
    }

    /**
     * @param InviteForm $dataForm
     */
    public function sendInvite(InviteForm $dataForm)
    {
        $user = $this->userCreator->create($dataForm->email, $dataForm->name, $dataForm->position_id);
        $companyUser = $this->companyUserCreator->create(CompanyHasUser::INVITED_STATUS,
            $this->currentUser->getWorkCompanyId());

        // Сохранение в бд, связанных данных через транзакцию
        $this->transactionManager->execute(function () use ($user, $companyUser) {
            $this->profileRepository->save($user);
            $companyUser->user_id = $user->user_id;
            $this->userCompanyRepository->save($companyUser);
        });

        //Выслать письмо
        $this->mailSender->send($user, $this->currentUser->getCompanyName());
    }

    /**
     * @param string $token
     */
    public function confirmInviteByToken(string $token)
    {
        $this->transactionManager->execute(function () use ($token) {
            if (!$this->inviteConfimer->confirm($token)) {
                throw new NotFoundException();
            }

            $invitedUser = $this->inviteConfimer->getUser();

            /** @var CompanyHasUser $userCompany */
            $userCompany = $invitedUser->invitedCompany;
            $userCompany->setStatus(CompanyHasUser::WORKING_STATUS);

            $this->userCompanyRepository->save($userCompany);
        });
    }

    /**
     * @param int $userId
     */
    public function confirmInviteByUser(int $userId)
    {
        $companyHasUser = $this->getCompanyHasUser($userId);
        $companyHasUser->status = CompanyHasUser::WORKING_STATUS;
        $this->userCompanyRepository->save($companyHasUser);
    }

    /**
     * @param int $userId
     */
    public function rejectInvite(int $userId)
    {
        $companyHasUser = $this->getCompanyHasUser($userId);
        (new InviteRejector($companyHasUser))->reject();
    }

    /**
     * @param int $userId
     */
    public function sendInviteAgain(int $userId)
    {
        $user = $this->profileRepository->findOne($userId);
        $user->generatePassword();
        $user->generateEmailToken();
        $this->profileRepository->save($user);

        $this->mailSender->send($user, $this->currentUser->getCompanyName());
    }

    /**
     * @param int $userId
     * @return CompanyHasUser
     */
    private function getCompanyHasUser(int $userId)
    {
        $companyId = $this->currentUser->getWorkCompanyId();

        return $this->userCompanyRepository->findOne($userId, $companyId);
    }
}