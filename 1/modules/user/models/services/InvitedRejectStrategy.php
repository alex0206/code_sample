<?php


namespace backend\modules\user\models\services;


use backend\modules\user\repositories\ProfileRepository;
use common\models\company\CompanyHasUser;

class InvitedRejectStrategy implements RejectInterface
{
    private $profileRepository;

    public function __construct(ProfileRepository $profileRepository)
    {
        $this->profileRepository = $profileRepository;
    }

    public function execute(CompanyHasUser $companyHasUser)
    {
        $user = $companyHasUser->user;
        $this->profileRepository->delete($user);
    }
}