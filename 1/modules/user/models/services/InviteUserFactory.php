<?php


namespace backend\modules\user\models\services;


use backend\modules\user\models\Profile;

class InviteUserFactory
{
    /**
     * @param string $email
     * @param string $name
     * @param int $position_id
     * @return Profile
     */
    public function create(string $email, string $name, int $position_id): Profile
    {
        $user = new Profile([
            'email'       => $email,
            'name'        => $name,
            'position_id' => $position_id,
        ]);

        $user->generateEmailToken();
        $user->generateAuthKey();
        $user->generatePassword();

        return $user;
    }
}