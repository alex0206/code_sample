<?php


namespace backend\modules\user\models\services;


use common\models\company\CompanyHasUser;
use Yii;
use yii\base\NotSupportedException;

class InviteRejector
{
    private $companyHasUser;

    public function __construct(CompanyHasUser $companyHasUser)
    {
        $this->companyHasUser = $companyHasUser;
    }

    /**
     * @return RejectInterface
     * @throws NotSupportedException
     */
    private function createStrategy(): RejectInterface
    {
        switch ($this->companyHasUser->status) {
            case CompanyHasUser::NEW_STATUS:
                $className = 'backend\modules\user\models\services\NewInviteRejectStrategy';
                break;
            case CompanyHasUser::INVITED_STATUS:
                $className = 'backend\modules\user\models\services\InvitedRejectStrategy';
                break;
            default:
                throw new NotSupportedException("The reject invite strategy by status: {$this->companyHasUser->status} is not supported");
        }

        /** @var RejectInterface $strategyObject */
        $strategyObject = Yii::createObject($className);

        return $strategyObject;
    }

    public function reject()
    {
        $strategyObject = $this->createStrategy();
        $strategyObject->execute($this->companyHasUser);
    }
}
