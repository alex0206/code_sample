<?php


namespace backend\modules\user\models\services;


use common\models\company\CompanyHasUser;

class CompanyUserFactory
{
    /**
     * @param string $status Status variant of CompanyHasUser model
     * @param int $company_id
     * @see CompanyHasUser For status variants
     * @return CompanyHasUser
     */
    public function create(string $status, int $company_id): CompanyHasUser
    {
        return new CompanyHasUser([
            'company_id' => $company_id,
            'status'     => $status,
        ]);
    }
}