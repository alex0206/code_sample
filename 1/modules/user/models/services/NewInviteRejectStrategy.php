<?php


namespace backend\modules\user\models\services;


use backend\modules\user\repositories\CompanyUserRepository;
use common\models\company\CompanyHasUser;

class NewInviteRejectStrategy implements RejectInterface
{
    private $companyUserRepository;

    public function __construct(CompanyUserRepository $companyUserRepository)
    {
        $this->companyUserRepository = $companyUserRepository;
    }

    public function execute(CompanyHasUser $companyHasUser)
    {
        $this->companyUserRepository->delete($companyHasUser);
    }
}