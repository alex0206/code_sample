<?php


namespace backend\modules\user\models\services;


use backend\modules\user\models\Profile;
use common\components\EmailSender;
use Yii;

class InviteMailNotifier
{
    const INVITE_CONFIRM_URL = '/user/recruit/invite-confirm';
    const EMAIL_TEMPLATE = 'userInvite';

    private $emailSender;

    public function __construct(EmailSender $emailSender)
    {
        $this->emailSender = $emailSender;
    }

    /**
     * @param Profile $user
     * @param string $companyName
     * @return bool
     */
    public function send(Profile $user, string $companyName): bool
    {
        $confirmLink = $this->createConfirmLink($user->getEmailToken());

        return $this->emailSender->send(self::EMAIL_TEMPLATE, $user->email, [
            'link'        => $confirmLink,
            'companyName' => $companyName,
            'password'    => $user->password,
        ],
            'Приглашение в компанию');
    }

    /**
     * @param string $token
     * @return string
     */
    private function createConfirmLink(string $token): string
    {
        return Yii::$app->urlManager->createAbsoluteUrl([self::INVITE_CONFIRM_URL, 'token' => $token]);
    }
}