<?php


namespace backend\modules\user\validators;


use common\modules\auth\models\SmsConfimer;
use yii\validators\Validator;

class ConfirmSmsCodeValidator extends Validator
{
    public function init()
    {
        parent::init();
        $this->message = 'Неверный код';
    }

    public function validateAttribute($model, $attribute)
    {
        $code = $model->$attribute;
        if (!(new SmsConfimer())->codeValidate($code)) {
            $model->addError($attribute, $this->message);
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        return <<<JS
            if (value.length == 4) {
                deferred.push($.get(
                    '/auth/sms/confirm-sms-code',
                    {code: value}
                ).done(function (json) {
                    if (json.result == 1) {
                        $('#profile-phone_confirm_code').parents('.input-row-content')
                            .find('.input-phone__confirm .ok').show().fadeOut(3000);
                    } else {
                        messages.push($message);
                    }
                }));
            }
JS;
    }
}