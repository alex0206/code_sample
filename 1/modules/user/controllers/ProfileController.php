<?php

namespace backend\modules\user\controllers;

use backend\modules\user\models\InviteForm;
use backend\modules\user\models\Profile;
use backend\modules\user\models\services\InviteService;
use backend\modules\user\models\services\ProfileService;
use backend\modules\user\repositories\CompanyUserRepository;
use backend\modules\user\repositories\ProfileRepository;
use common\exceptions\NotFoundException;
use common\models\company\CompanyHasUser;
use DomainException;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for Profile model.
 */
class ProfileController extends Controller
{
    private $profileService;
    /** @var ProfileRepository profileRepository */
    private $profileRepository;

    public function __construct($id, $module, ProfileService $profileService, array $config = [])
    {
        $this->profileService = $profileService;
        $this->profileRepository = $profileService->getRepository();
        parent::__construct($id, $module, $config);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all working users.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = $this->profileService->getDataProvider(CompanyHasUser::WORKING_STATUS);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all fired users.
     * @return mixed
     */
    public function actionFired()
    {
        if (!Yii::$app->request->isAjax) {
            return $this->redirect('index');
        }

        $dataProvider = $this->profileService->getDataProvider(CompanyHasUser::FIRED_STATUS);

        return $this->renderAjax('_grid', [
            'dataProvider' => $dataProvider,
            'pjaxId'       => 'pjax-fired-users',
            'emptyText'    => 'Никого пока не увольняли',
        ]);
    }

    public function actionRecruits()
    {
        if (!Yii::$app->request->isAjax) {
            return $this->redirect('index');
        }

        $newDataProvider = $this->profileService->getDataProvider(CompanyHasUser::NEW_STATUS);
        $invitedDataProvider = $this->profileService->getDataProvider(CompanyHasUser::INVITED_STATUS);
        $inviteForm = new InviteForm();

        return $this->renderAjax('_recruits', compact('newDataProvider', 'invitedDataProvider', 'inviteForm'));
    }

    /**
     * Displays a single Profile model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing Profile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->profileService->getModel($id);

        if (!$this->canEditProfile($model)) {
            return $this->redirect(['view', 'id' => $id]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->profileService->update($model);

            return $this->redirect(['view', 'id' => $id]);
        }

        $logo = $this->profileService->getPictureModel($model);

        return $this->render('update', [
            'model' => $model,
            'logo'  => $logo,
        ]);
    }

    /**
     * Fire an employee of the company
     * @param int $id User id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionRemove($id)
    {
        try {
            $this->profileService->fire($id);
        } catch (NotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage());
        } catch (DomainException $e) {
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    //@todo Создать rbac правило
    private function canEditProfile(Profile $model): bool
    {
        try {
            if ($model->user_id === user()->user_id) {
                return true;
            }

            $companyHasUser = (new CompanyUserRepository())->findOne($model->user_id, user()->getWorkCompanyId());

            if (!$companyHasUser->isFired() && !$companyHasUser->isNewInvite()) {
                return true;
            }
        } catch (NotFoundException $e) {
        }

        return false;
    }

    /**
     * Finds the Profile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\modules\user\models\Profile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        try {
            return $this->profileRepository->findOne($id);
        } catch (NotFoundException $e) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
