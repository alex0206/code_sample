<?php


namespace backend\modules\user\controllers;


use backend\modules\user\models\InviteForm;
use backend\modules\user\models\services\InviteService;
use backend\modules\user\models\services\ProfileService;
use common\exceptions\NotFoundException;
use common\models\company\CompanyHasUser;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class RecruitController extends Controller
{
    private $inviteService;
    private $profileService;

    public function __construct(
        $id,
        $module,
        InviteService $inviteService,
        ProfileService $profileService,
        array $config = []
    ) {
        $this->inviteService = $inviteService;
        $this->profileService = $profileService;

        parent::__construct($id, $module, $config);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['invite-confirm'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionInvite()
    {
        if (!Yii::$app->request->isPjax) {
            return false;
        }

        $model = new InviteForm();
        $message = null;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->inviteService->sendInvite($model);
            $message = 'Сотруднику выслано приглашение';
            $model->clearFields();
        }

        return $this->renderAjax('/profile/_inviteForm', compact('model', 'message'));
    }

    /**
     * @param string $token
     * @throws NotFoundHttpException
     */
    public function actionInviteConfirm(string $token)
    {
        try {
            $this->inviteService->confirmInviteByToken($token);
            $this->redirect(['/user/profile/update', 'id' => user()->user_id]);
        } catch (NotFoundException $e) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionInvitedList()
    {
        $invitedDataProvider = $this->profileService->getDataProvider(CompanyHasUser::INVITED_STATUS);

        return $this->renderAjax('_invitedGrid', ['dataProvider' => $invitedDataProvider]);
    }

    public function actionNewList()
    {
        $newDataProvider = $this->profileService->getDataProvider(CompanyHasUser::NEW_STATUS);

        return $this->renderAjax('_newRequestGrid', ['dataProvider' => $newDataProvider]);
    }
}