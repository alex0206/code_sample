<?php


namespace backend\modules\user\controllers;


use backend\modules\user\models\services\InviteService;
use backend\modules\user\models\services\ProfileService;
use backend\modules\user\repositories\ProfileRepository;
use common\behaviors\AjaxResponse;
use common\exceptions\NotFoundException;
use RuntimeException;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class InviteButtonsController
 * Actions for invite control buttons.
 * @package backend\modules\user\controllers
 */
class InviteButtonsController extends Controller
{
    private $inviteService;

    public function __construct($id, $module, InviteService $inviteService, array $config = [])
    {
        $this->inviteService = $inviteService;
        parent::__construct($id, $module, $config);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access'       => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'ajaxResponse' => [
                'class' => AjaxResponse::className(),
            ],
        ];
    }

    /**
     * @param int $id User id
     * @return array
     */
    public function actionConfirm(int $id)
    {
        return $this->executeRequest(function () use ($id) {
            $this->inviteService->confirmInviteByUser($id);
        }, 'confirm_invite_by_user');
    }

    /**
     * @param int $id User id
     * @return array
     */
    public function actionSendAgain(int $id)
    {
        return $this->executeRequest(function () use ($id) {
            $this->inviteService->sendInviteAgain($id);
        }, 'invite_send_again');
    }

    /**
     * @param int $id User id
     * @return array
     */
    public function actionRemove(int $id)
    {
        return $this->executeRequest(function () use ($id) {
            $this->inviteService->rejectInvite($id);
        }, 'reject_invite');
    }

    /**
     * @param callable $function
     * @param string $categoryRequest
     * @return array
     */
    private function executeRequest(callable $function, string $categoryRequest)
    {
        try {
            $response = ['status' => 200, 'message' => 'Ok'];
            call_user_func($function);
        } catch (NotFoundException $e) {
            $response = ['status' => 404, 'message' => $e->getMessage()];
        } catch (RuntimeException $e) {
            $response = ['status' => 500, 'message' => $e->getMessage()];
            Yii::error(['message' => $response['message'], 'trace' => $e->getTraceAsString()], $categoryRequest);
        }

        return $response;
    }
}