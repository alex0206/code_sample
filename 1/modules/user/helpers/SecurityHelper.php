<?php


namespace backend\modules\user\helpers;


use Yii;

class SecurityHelper
{
    public static function passwordGenerate($length = 10)
    {
        return Yii::$app->security->generateRandomString($length);
    }
}