<?php


namespace backend\modules\user\assets\profile;


use yii\web\AssetBundle;

class ProfileAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/user/assets/profile/source';
    public $publishOptions = [
        'forceCopy' => true,
    ];
    public $css = [
    ];
    public $js = [
        'js/app.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'backend\modules\user\assets\common\CommonAsset',
        'yii\widgets\MaskedInputAsset',
    ];
}