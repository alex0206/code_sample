;(function () {
    'use strict';

    var app = {
        sensSmsAction: '/auth/sms/send-sms-code',
        oldPhone: null,
        phoneSelector: '#profile-phone',
        init: function () {
            this.initOldPhone();
            this.sendSmsEventListener();
            this.changePhoneEventListener();
        },
        sendSmsEventListener: function () {
            var _this = this;
            $('.input-phone__confirm a').on('click', function (e) {
                e.preventDefault();
                var blockSmsCssClass = 'sent';
                var link = $(this);
                var phone = _this._getPhoneValue();
                if (!link.hasClass(blockSmsCssClass)) {
                    $.post(
                        _this.sensSmsAction,
                        {phone: phone},
                        function (json) {
                            if (json.result == 1) {
                                link.addClass(blockSmsCssClass);

                                countdownSeconds(link.find('span'), 60, function () {
                                    link.removeClass(blockSmsCssClass);
                                });
                            }
                        }
                    );
                }
            });
        },
        initOldPhone: function () {
            this.oldPhone = this._getPhoneValue();
        },
        _getPhoneValue: function () {
            return $(this.phoneSelector).inputmask('unmaskedvalue');
        },
        changePhoneEventListener: function () {
            var _this = this;
            $(this.phoneSelector).on('change.yii', function () {
                var phone = _this._getPhoneValue();

                if (_this.oldPhone !== phone) {
                    $('.field-profile-phone_confirm_code').show();
                } else {
                    $('.field-profile-phone_confirm_code').hide();
                }
            });
        }
    };

    app.init();
})();