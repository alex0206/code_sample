<?php


namespace backend\modules\user\assets\common;


use yii\web\AssetBundle;

class CommonAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/user/assets/common/source';
    public $publishOptions = [
        'forceCopy' => true,
    ];
    public $css = [
    ];
    public $js = [
        'js/functions.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}