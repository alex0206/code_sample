function countdownSeconds(secondsJqObject, seconds, afterClearInterval) {
    secondsJqObject.text(seconds);
    var intervalId = setInterval(function () {
        var curSeconds = parseInt(secondsJqObject.text());
        curSeconds--;
        if (curSeconds === 0) {
            clearInterval(intervalId);
            if (afterClearInterval !== undefined) {
                afterClearInterval.call();
            }
            secondsJqObject.text('');
        } else {
            secondsJqObject.text(curSeconds);
        }
    }, 1000);
}