<?php

namespace backend\modules\user\assets\inviteButtons;

use yii\web\AssetBundle;

class InviteButtonsAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/user/assets/inviteButtons/source';
    public $publishOptions = [
        'forceCopy' => true,
    ];
    public $css = [
    ];
    public $js = [
        'js/app.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'backend\modules\user\assets\common\CommonAsset',
    ];
}