;(function () {
    'use strict';

    var app = {
        init: function () {
            this.inviteSentAgainEventListener();
            this.confirmInviteEventListener();
            this.rejectInviteEventListener();
        },
        inviteSentAgainEventListener: function () {
            $(document).on('click', 'table a.js-invite-again', function (e) {
                e.preventDefault();
                var link = $(this);
                if (!link.hasClass('sent')) {
                    $.getJSON(link.attr('href'), {}, function (json) {
                        if (json.status == 200) {
                            alert('Повторное приглашение успешно отправлено');
                            link.addClass('sent');
                            countdownSeconds(link.find('span'), 60, function () {
                                link.removeClass('sent');
                            });
                        }
                    });
                }
            });
        },
        confirmInviteEventListener: function () {
            $(document).on('click', 'table a.js-invite-confirm', function (e) {
                e.preventDefault();
                var button = $(this);
                $.getJSON(button.attr('href'), {}, function (json) {
                    if (json.status == 200) {
                        button.parents('tr').remove();
                    }
                });
            });
        },
        rejectInviteEventListener: function () {
            $(document).on('click', 'table a.js-invite-remove', function (e) {
                e.preventDefault();
                var button = $(this);
                $.getJSON(button.attr('href'), {}, function (json) {
                    if (json.status == 200) {
                        button.parents('tr').remove();
                    }
                });
            });
        }
    };

    app.init();
})();