<?php
/* @var $this \yii\web\View */
/* @var $photos \common\modules\photo\models\Photo[] */
/* @var $plan \common\modules\photo\models\Photo[] */
/* @var $buildingPhotos \common\modules\photo\models\Photo[] */


$arResult = array_slice($photos, 0, 5);

$firstPlanPhoto = current($plan);
$firstBuildingPhoto = current($buildingPhotos);
$isEmptyPlanPhoto = empty($firstPlanPhoto);
$isEmptyBuildingPhoto = empty($firstBuildingPhoto);

$totalDeleteElement = 0;

if (!$isEmptyPlanPhoto) {
    $totalDeleteElement++;
}

if (!$isEmptyBuildingPhoto) {
    $totalDeleteElement++;
}

//Чистим основные фотки под место след. категорий фото
for ($i = 0; $i < $totalDeleteElement; $i++) {
    if (count($arResult) > 3) {
        array_pop($arResult);
    }
}

if (!$isEmptyPlanPhoto) {
    $arResult[] = $firstPlanPhoto;
}

if (!$isEmptyBuildingPhoto) {
    $arResult[] = $firstBuildingPhoto;
}

$total = count($arResult);
?>

<div class="objects__item__mini-slider slides-<?= $total ?>" data-slides="<?= $total ?>">
    <a class="icon icon__star"></a>
    <?php
    $links = '';
    for ($i = 0; $i < $total; $i++):
        $active = $i === 0 ? 'active' : '';
        $links .= "<a data-slide-id=\"$i\" class=\"$active\"></a>\n";
        ?>
        <div class="objects__item__mini-slider__item <?= $active ?>"
             style="background-image: url('<?= $arResult[$i]->getBigFilePath() ?>');"
             data-slide-id="<?= $i ?>"></div>
    <?php endfor; ?>

    <div class="objects__item__mini-slider__nav">
        <?= $links ?>
    </div>
</div>