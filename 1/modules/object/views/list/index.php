<?php
/* @var $this \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel \backend\modules\object\models\ObjectSearch */
/* @var $isSerching bool */

use backend\modules\object\assets\filter\FilterAsset;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

$this->title = 'Объекты';

$this->registerAssetBundle(FilterAsset::className());
?>
<?= Html::a('+ Добавить', ['/object/persist/create'], ['class' => 'title-right-link']) ?>
    <h1><?= $this->title ?></h1>

<?php if ($dataProvider->getCount() > 0 || $isSerching): ?>

    <div class="right-links">
        <!--        <a class="active">Списком</a>-->
        <!--        <a>На карте</a>-->
    </div>
    <section class="island">
        <?php Pjax::begin() ?>
        <?= $this->render('_filter', ['model' => $searchModel]) ?>
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView'     => '_item',
            'itemOptions'  => ['class' => 'objects__item'],
            'options'      => ['class' => 'objects'],
            'layout'       => "{items}\n{pager}",
        ]); ?>
        <?php Pjax::end() ?>
    </section>
<?php else: ?>
    <p>Добавьте первый объект</p><br>
    <?= Html::a('Добавить объект', ['/object/persist/create'], ['class' => 'button']) ?>
<?php endif; ?>