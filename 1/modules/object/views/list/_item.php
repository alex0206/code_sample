<?php
/* @var $this \yii\web\View */
/* @var $model \backend\modules\object\models\Object, the data model */
/* @var $key mixed, the key value associated with the data item */
/* @var $index integer, the zero-based index of the data item in the items array returned by [[dataProvider]]. */
/* @var $widget \yii\widgets\ListView, this widget instance */

use common\models\object\Object;
use yii\helpers\Html;

?>
<?= $this->render('_preview',
    ['photos' => $model->photos, 'plan' => $model->plan, 'buildingPhotos' => $model->buildingPhotos]) ?>
<div class="objects__item__content">
    <div class="objects__item__content__user">
        <?= $this->render('_userBlock', ['user' => $model->createdBy]) ?>
    </div>
    <div class="objects__item__content__title">
        <?= Html::a(Html::encode($model->street . ' ' . $model->build_number . '/' . $model->apt_number),
            ['/object/list/view', 'id' => $model->id], ['data-pjax' => 0]) ?>
        <span><?= Html::encode($model->city) ?></span>
    </div>
    <div class="objects__item__content__cols">
        <div class="col">
            <b><?= $model->room_number ?>-комнатная</b>
            <?php if ($model->is_penthouse): ?>
                <span>Пейнтхаус</span>
            <?php endif; ?>
            <?php if ($model->is_apartment): ?>
                <span>Аппартаменты</span>
            <?php endif; ?>
            <?php if ($model->management_company_name): ?>
                <span><?= Html::encode($model->management_company_name) ?></span>
            <?php endif; ?>
            <?php if ($model->build_type): ?>
                <span><?= Html::encode($model->getBuildTypeName()) ?><?php if ($model->build_serial): ?>, <?= Html::encode($model->build_serial) ?><?php endif; ?></span>
            <?php endif; ?>
        </div>
        <div class="col">
            <b><?= $model->total_square ?> м<sup>2</sup></b>
            <?php if ($model->kitchen_square): ?>
                <span>Кухня <?= $model->kitchen_square ?> м<sup>2</sup></span>
            <?php endif; ?>
            <span>Жилая <?= $model->room_square ?> м<sup>2</sup></span>
        </div>
        <div class="col">
            <b><?= $model->floor ?>/<?= $model->max_floor ?> этаж</b>
            <?php if ($model->loggia_number): ?>
                <span>Лоджия: <?= $model->loggia_number ?> шт.</span>
            <?php endif; ?>
            <?php if ($model->balcony_number): ?>
                <span>Балкон: <?= $model->balcony_number ?> шт.</span>
            <?php endif; ?>
            <?php if ($model->passenger_lift_count): ?>
                <span>Грузовой лифт: <?= $model->passenger_lift_count ?> шт.</span>
            <?php endif; ?>
            <?php if ($model->service_lift_count): ?>
                <span>Пассажирский лифт: <?= $model->service_lift_count ?> шт.</span>
            <?php endif; ?>
        </div>
        <div class="col">
            <?php if ($model->price): ?>
                <b><?= $model->getFormatedPrice() ?> ₽</b>
                <span><?= $model->getSquarePrice() ?> ₽/м<sup>2</sup></span>
                <?php if ($model->enable_realtor_work): ?>
                    <span><i class="icon icon__hands"></i>
                        <?php if ($model->isMoneyRealtorCommissionType()): ?>
                            <?= $model->realtor_commission ?> ₽
                        <?php else: ?>
                            <?= $model->realtor_commission ?>% (<?= $model->getRealtorPrice() ?> ₽)
                        <?php endif; ?>
                </span>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="objects__item__content__description">
        <?= Html::encode($model->description) ?>
    </div>
    <div class="objects__item__content__details">
        <?php if ($model->egrp): ?>
            <div>
                <i class="icon icon__ok"></i>ЕГРП
            </div>
        <?php endif; ?>
        <?php if ($model->selling_type === Object::SELLING_TYPE_FREE): ?>
            <div>
                <i class="icon icon__ok"></i>Свободная продажа
            </div>
        <?php endif; ?>
        <div class="right-item">
            <span class="date"><?= $model->getCreateDate() ?></span>
        </div>
    </div>
</div>


