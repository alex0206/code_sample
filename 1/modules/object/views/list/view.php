<?php
/* @var $this \yii\web\View */
/* @var $model \backend\modules\object\models\Object, the data model */
/* @var $dataEventProvider \yii\data\ActiveDataProvider */
/* @var $eventForm \backend\modules\object\models\forms\EventForm */

use common\modules\photo\models\Photo;
use yii\helpers\Html;

?>
<h1 class="with-go-back">
    <?= Html::a('', ['index'], ['class' => 'icon icon__go-back']) ?>
    <?= Html::encode($model->street . ' ' . $model->build_number . '/' . $model->apt_number) ?>
    <span><?= Html::encode($model->city) ?></span>
</h1>
<section class="island">
    <div class="input-row object-view-header">
        <div class="inline-blocks middle-blocks">
            <div class="inline-blocks__item mini-item">
                <div class="object-view__h1"><?= $model->getFormatedPrice() ?> ₽</div>
                <div class="object-view__h2"><?= $model->getSquarePrice() ?> ₽/м²</div>
            </div>
            <?php if ($model->enable_realtor_work): ?>
                <div class="inline-blocks__item">
                    <div class="object-view__h2"><i class="icon icon__big-hands"></i>
                        <?php if ($model->isMoneyRealtorCommissionType()): ?>
                            <?= $model->realtor_commission ?> ₽
                        <?php else: ?>
                            <?= $model->realtor_commission ?>% (<?= $model->getRealtorPrice() ?> ₽)
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="inline-blocks__item right-item">
                <?= $this->render('_userBlock', ['user' => $model->createdBy]) ?>
            </div>
        </div>
    </div>
    <div class="object-view">
        <div class="object-view__left">
            <div class="input-row">
                <table>
                    <tr>
                        <th style="width: 50%;">Квартира</th>
                        <th style="width: 50%;"></th>
                    </tr>
                    <tr>
                        <td>Количество комнат</td>
                        <td><?= Html::encode($model->room_number) ?></td>
                    </tr>
                    <tr>
                        <td>Площадь</td>
                        <td>Общая <?= Html::encode($model->total_square) ?> м<sup>2</sup>/
                            Жилая <?= Html::encode($model->room_square) ?> м<sup>2</sup>
                            <?php if ($model->kitchen_square): ?>/ Кухня <?= Html::encode($model->kitchen_square) ?> м
                                <sup>2</sup><?php endif; ?></td>
                    </tr>
                    <tr>
                        <td>Этаж</td>
                        <td><?= Html::encode($model->floor) ?>/<?= Html::encode($model->max_floor) ?></td>
                    </tr>
                    <tr>
                        <td>Лоджия</td>
                        <td><?php if ($model->loggia_number): ?>Есть<?php else: ?>Нет<?php endif; ?></td>
                    </tr>
                    <tr>
                        <td>Балкон</td>
                        <td><?php if ($model->balcony_number): ?>Есть<?php else: ?>Нет<?php endif; ?></td>
                    </tr>
                    <tr>
                        <td>Окна выходят</td>
                        <td><?php if ($model->is_yard_window): ?>Во двор<?php elseif ($model->is_street_window): ?>На улицу<?php endif; ?></td>
                    </tr>
                    <tr>
                        <td>Санузел</td>
                        <td><?= $model->separate_toilets_count ?> раздельный,<br><?= $model->combined_toilets_count ?>
                            совмещённый
                        </td>
                    </tr>
                    <tr>
                        <td>Телефон</td>
                        <td><?php if ($model->has_phone): ?>Есть<?php else: ?>Нет<?php endif; ?></td>
                    </tr>
                    <tr>
                        <td>Ремонт</td>
                        <td><?= $model->getRepairTypeName() ?></td>
                    </tr>
                </table>
            </div>
            <div class="input-row">
                <table>
                    <tr>
                        <th style="width: 50%;">Дом</th>
                        <th style="width: 50%;"></th>
                    </tr>
                    <tr>
                        <td>Название</td>
                        <td><?= Html::encode($model->management_company_name) ?></td>
                    </tr>
                    <tr>
                        <td>Год постройки</td>
                        <td><?= Html::encode($model->build_year) ?></td>
                    </tr>
                    <tr>
                        <td>Тип и серия дома</td>
                        <td><?= $model->getBuildTypeName() ?><?php if ($model->build_serial): ?>, <?= Html::encode($model->build_serial) ?><?php endif; ?></td>
                    </tr>
                    <tr>
                        <td>Высота потолков</td>
                        <td><?= Html::encode($model->ceiling_height) ?> м</td>
                    </tr>
                    <tr>
                        <td>Лифт</td>
                        <td>
                            <?php
                            $res = '';

                            if ($model->passenger_lift_count) {
                                $res = 'Пассажирский';
                            }

                            if ($model->service_lift_count) {
                                $res .= $res ? ' и грузовой' : 'Грузовой';
                            }

                            echo $res ? $res : 'Нет';
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Дополнительно</td>
                        <td><?php if ($model->has_garbage_chute): ?>Мусоропровод<?php endif; ?></td>
                    </tr>
                    <tr>
                        <td>Парковка</td>
                        <td><?= $model->getParkingTypeName() ?></td>
                    </tr>
                </table>
            </div>
            <div class="input-row"><span><?= Html::encode($model->description) ?></span></div>
        </div>
        <div class="object-view__right">
            <div class="object-view__select-view">
                <?php //todo Добавить фото ?>
                <a class="active">Фото</a>
                <a>Карта</a>
                <a>Панорама</a>
                <div>
                    <?php
                    /** @var Photo $photo */
                    foreach ($model->photos as $photo): ?>
                        <?= Html::a('', $photo->getBigFilePath()) ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="island">
    <div class="tabs">
        <div class="tabs__header">
            <div class="tabs__header__item tabs-active" data-tab-id="t01">
                События и задачи
            </div>
            <!--            <div class="tabs__header__item" data-tab-id="t02">-->
            <!--                Клиент и статус-->
            <!--            </div>-->
            <!--            <div class="tabs__header__item" data-tab-id="t03">-->
            <!--                Реклама-->
            <!--            </div>-->
            <!--            <div class="tabs__header__item" data-tab-id="t04">-->
            <!--                Проверка-->
            <!--            </div>-->
            <!--            <div class="tabs__header__item" data-tab-id="t05">-->
            <!--                Похожие-->
            <!--            </div>-->
        </div>
        <div class="tabs__content">
            <div class="tabs__content__item tabs-active" data-tab-id="t01">
                <?= $this->render('/event/index', [
                    'dataEventProvider' => $dataEventProvider,
                    'eventForm'         => $eventForm,
                    'object_id'         => $model->id,
                ]) ?>
            </div>
            <!--            <div class="tabs__content__item" data-tab-id="t02">-->
            <!--                2-->
            <!--            </div>-->
            <!--            <div class="tabs__content__item" data-tab-id="t03">-->
            <!--                3-->
            <!--            </div>-->
            <!--            <div class="tabs__content__item" data-tab-id="t04">-->
            <!--                4-->
            <!--            </div>-->
            <!--            <div class="tabs__content__item" data-tab-id="t05">-->
            <!--                5-->
            <!--            </div>-->
        </div>
    </div>
</section>
