<?php
/* @var $this \yii\web\View */
/* @var $user \backend\modules\user\models\Profile */
/* @var $photo \common\modules\photo\models\Photo */

use yii\helpers\Html;

$photo = $user->photo;
?>
<div class="user-block">
    <div class="user-block__title">
        <div class="photo" style="background-image: url('<?= $photo ? $photo->getSmallFilePath() : null ?>');"></div>
        <b><?= Html::encode($user->name) ?></b>
        <span><?= Html::encode($user->company->brand_name) ?></span>
    </div>
    <div class="user-block__content">
        <ul>
            <li>
                <?= Html::a('Перейти в профиль',
                    ['/user/profile/update/', 'id' => $user->user_id], ['data-pjax' => 0]) ?>
            </li>
            <li>
                <?= Html::a('Перейти в компанию',
                    ['/company/profile/view/', 'id' => $user->company->company_id], ['data-pjax' => 0]) ?>
            </li>
            <li>
                <?php //@todo Функция "Написать" не реализована?>
                <a href="#">Написать (Не реализовано)</a>
            </li>
            <li>
                <?php //@todo Функция "Пожаловаться" не реализована?>
                <a href="#">Пожаловаться (Не реализовано)</a>
            </li>
        </ul>
    </div>
</div>
