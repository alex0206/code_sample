<?php
/* @var $this \yii\web\View */
/* @var $model \backend\modules\object\models\ObjectSearch */

use common\models\object\ObjectDirectory;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin([
    'method'                 => 'get',
    'enableClientValidation' => false,
    'options'                => ['data-pjax' => true],
    'fieldConfig'            => [
        'template' => '{input}',
        'options'  => ['class' => 'inline-blocks__item mini-item'],
    ],
]) ?>
    <div class="input-row objects-filter">
        <div class="inline-blocks" style="position: relative;">
            <?= $form->field($model, 'offer_type', [
                'options' => ['class' => 'objects-filter__item'],
            ])->dropDownList(array_slice(ObjectDirectory::getOfferTypeMap(), 0, 1), ['class' => 'js-select']) ?>
            <div class="objects-filter__item">
                <div class="custom-select objects-filter-custom-select">
                    <div class="custom-select__title">
                        Квартира
                    </div>
                    <div class="custom-select__content">
                        <div class="objects-filter-type">
                            <?php
                            $offerTypeMap = ObjectDirectory::getOwnershipTypeMap();
                            foreach ($offerTypeMap as $key => $name):
                                ?>
                                <div class="type"><?= $name ?></div>
                                <div class="items">
                                    <?= $form->field($model,
                                        'type')->radioList(ObjectDirectory::getTypeMapByOfferType($key),
                                        [
                                            'item' => function ($index, $label, $name, $checked, $value) use ($key) {
                                                $input_id = $key . '_' . $name . '_' . $index;
                                                $return = '<input id="' . $input_id . '" type="radio" name="' . $name .
                                                    '" value="' . $value . '" tabindex="3"><label for="' . $input_id . '">' . ucwords($label) . '</label>';

                                                return $return;
                                            },
                                            'tag'  => false,
                                        ]
                                    ) ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="objects-filter__item">
                <div class="custom-select objects-filter-custom-select">
                    <div class="custom-select__title">
                        Комнаты
                    </div>
                    <div class="custom-select__content">
                        <div class="objects-filter-rooms">
                            <?= $form->field($model, 'room_number')->checkboxList(ObjectDirectory::getRoomMap()) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="objects-filter__item">
                <div class="custom-select objects-filter-custom-select">
                    <div class="custom-select__title">
                        Цена
                    </div>
                    <div class="custom-select__content price-filter">
                        <div class="objects-filter-price">
                            <div class="from">
                                <span>от</span>
                                <?= $form->field($model, 'min_price')->textInput(['placeholder' => 'Цена от']) ?>
                                <span>₽</span>
                            </div>
                            <div class="to">
                                <span>до</span>
                                <?= $form->field($model, 'max_price')->textInput(['placeholder' => 'Цена до']) ?>
                                <span>₽</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="objects-filter__submit">
                <?= Html::submitButton('Искать', ['class' => 'button']) ?>
            </div>
        </div>
    </div>
    <div class="input-row object-search">
        <div class="inline-blocks">
            <?= $form->field($model, 'search',
                ['options' => ['class' => 'inline-blocks__item mini-item right-item']])->textInput(['placeholder' => 'Поиск']) ?>

            <?= $form->field($model, 'sort', [
                'options'  => [
                    'class' => 'inline-blocks__item sort-item',
                ],
                'template' => "Сортировать:\n{input}",
            ])->dropDownList([
                'price'  => 'По цене от меньшей',
                '-price' => 'По цене от большей',
            ], ['name' => 'sort', 'class' => 'js-select']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>