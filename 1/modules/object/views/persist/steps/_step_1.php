<?php
/* @var $this \yii\web\View */
/* @var $model \backend\modules\object\models\forms\ObjectForm */

use common\models\object\ObjectDirectory;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div data-wizard-id="0" class="input-wizard__content__item active-wizard">
    <?php $form = ActiveForm::begin() ?>
    <div class="input-row">
        <div class="inline-blocks">
            <?= $form->field($model, 'offer_type', [
                'options'  => ['class' => 'inline-blocks__item'],
                'template' => "<div class=\"styled-radio\">\n{input}\n{error}</div>",
            ])->radioList(ObjectDirectory::getOfferTypeMap(), [
                'tag'  => false,
                'item' => function ($index, $label, $name, $checked, $value) {
                    $id = "{$name}_{$index}";
                    $input = Html::input('radio', $name, $value, ['id' => $id, 'checked' => $checked]);
                    $label = Html::label($label, $id);

                    return Html::label($input . $label, null, ['class' => 'styled-radio__item']);
                },
            ]) ?>

            <?= $form->field($model, 'ownership_type', [
                'options'  => ['class' => 'inline-blocks__item'],
                'template' => "<div class=\"styled-radio\">\n{input}\n{error}</div>",
            ])->radioList(ObjectDirectory::getOwnershipTypeMap(), [
                'tag'  => false,
                'item' => function ($index, $label, $name, $checked, $value) {
                    $id = "{$name}_{$index}";
                    $input = Html::input('radio', $name, $value, ['id' => $id, 'checked' => $checked]);
                    $label = Html::label($label, $id);

                    return Html::label($input . $label, null, ['class' => 'styled-radio__item']);
                },
            ]) ?>

            <?= $form->field($model, 'type', [
                'options'  => ['class' => 'inline-blocks__item'],
                'template' => "{input}\n{error}",
            ])->dropDownList(ObjectDirectory::getTypeMap(false), ['class' => 'js-select']) ?>
        </div>
    </div>

    <?= $form->field($model, 'city', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}<div class=\"input-row-content\">{input}\n{error}</div>",
    ])->textInput() ?>

    <!--
    <?= $form->field($model, 'street', [
        'options'  => ['class' => 'input-row'],
        'template' => '{label}<div class="input-row-content">' .
            "<div style=\"float: right; line-height: 39px;\"><a>На карте</a></div><div style=\"margin-right: 90px\">{input}\n{error}</div></div>",
    ])->textInput() ?>
    -->

    <div class="input-row object-address">
        <label>Адрес объекта</label>
        <div class="input-row-content">
            <div class="object-address__street">
                <input type="text" placeholder="Улица">
            </div>
            <div class="object-address__mini">
                <input type="text" placeholder="Дом">
            </div>
            <div class="object-address__mini">
                <input type="text" placeholder="Корпус">
            </div>
            <div class="object-address__mini">
                <input type="text" placeholder="Кв.">
            </div>
            <div class="object-address__map">
                <a>На карте</a>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'build_number', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}<div class=\"input-row-content\">{input}\n{error}</div>",
    ])->textInput() ?>

    <?= $form->field($model, 'housing', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}<div class=\"input-row-content\">{input}\n{error}</div>",
    ])->textInput() ?>

    <?= $form->field($model, 'apt_number', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}<div class=\"input-row-content\">{input}\n{error}</div>",
    ])->textInput() ?>

    <?= $form->field($model, 'cadastral_number', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}<div class=\"input-row-content\">{input}\n{error}</div>",
    ])->textInput() ?>

    <div class="input-row">
        <?= Html::activeLabel($model, 'room_number') ?>
        <div class="input-row-content">
            <div class="inline-blocks">
                <?= $form->field($model, 'room_number', [
                    'options'  => ['class' => 'inline-blocks__item mini-item'],
                    'template' => "{input}\n{error}",
                ])->dropDownList(ObjectDirectory::getRoomNumberList(), ['class' => 'js-select']) ?>
                <?= $form->field($model, 'is_apartment',
                    ['options' => ['class' => 'inline-blocks__item']])->checkbox() ?>
                <?= $form->field($model, 'is_penthouse',
                    ['options' => ['class' => 'inline-blocks__item']])->checkbox() ?>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'total_square', [
        'options'  => ['class' => 'input-row'],
        'template' => '{label}<div class="input-row-content">' .
            "<div class=\"inline-blocks\"><div class=\"inline-blocks__item mini-item\">{input}\n{error}</div>" .
            '<div class="inline-blocks__item">м<sup>2</sup></div></div></div>',
    ])->textInput() ?>

    <div class="input-row">
        <?= Html::activeLabel($model, 'floor') ?>
        <div class="input-row-content">
            <div class="inline-blocks">
                <div class="inline-blocks__item mini-item">
                    <div class="inline-blocks col-3">
                        <?= $form->field($model, 'floor', [
                            'options'  => ['class' => 'inline-blocks__item'],
                            'template' => "{input}\n{error}",
                        ])->textInput(['class' => 'numeric', 'placeholder' => '0']) ?>
                        <div class="inline-blocks__item" style="text-align: center;">
                            из
                        </div>
                        <?= $form->field($model, 'max_floor', [
                            'options'  => ['class' => 'inline-blocks__item'],
                            'template' => "{input}\n{error}",
                        ])->textInput(['class' => 'numeric', 'placeholder' => '0']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'room_square', [
        'options'  => ['class' => 'input-row'],
        'template' => '{label}<div class="input-row-content">' .
            "<div class=\"inline-blocks\"><div class=\"inline-blocks__item mini-item\">{input}\n{error}</div>" .
            '<div class="inline-blocks__item">м<sup>2</sup></div></div></div>',
    ])->textInput() ?>

    <?= $form->field($model, 'kitchen_square', [
        'options'  => ['class' => 'input-row'],
        'template' => '{label}<div class="input-row-content">' .
            "<div class=\"inline-blocks\"><div class=\"inline-blocks__item mini-item\">{input}\n{error}</div>" .
            '<div class="inline-blocks__item">м<sup>2</sup></div></div></div>',
    ])->textInput(['class' => 'numeric', 'placeholder' => '0']) ?>

    <?= $form->field($model, 'loggia_number', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}<div class=\"input-row-content\"><div class=\"styled-radio\">\n{input}\n{error}</div></div>",
    ])->radioList(ObjectDirectory::getLoggiaNumberList(), [
        'tag'  => false,
        'item' => function ($index, $label, $name, $checked, $value) {
            $id = "{$name}_{$index}";
            $input = Html::input('radio', $name, $value, ['id' => $id, 'checked' => $checked]);
            $label = Html::label($label, $id);

            return Html::label($input . $label, null, ['class' => 'styled-radio__item']);
        },
    ]) ?>

    <?= $form->field($model, 'balcony_number', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}<div class=\"input-row-content\"><div class=\"styled-radio\">\n{input}\n{error}</div></div>",
    ])->radioList(ObjectDirectory::getBalconyNumberList(), [
        'tag'  => false,
        'item' => function ($index, $label, $name, $checked, $value) {
            $id = "{$name}_{$index}";
            $input = Html::input('radio', $name, $value, ['id' => $id, 'checked' => $checked]);
            $label = Html::label($label, $id);

            return Html::label($input . $label, null, ['class' => 'styled-radio__item']);
        },
    ]) ?>

    <div class="input-row">
        <label>Окна выходят</label>
        <div class="input-row-content">
            <div class="inline-blocks">
                <?= $form->field($model, 'is_yard_window',
                    ['options' => ['class' => 'inline-blocks__item']])->checkbox() ?>
                <?= $form->field($model, 'is_street_window',
                    ['options' => ['class' => 'inline-blocks__item']])->checkbox() ?>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'separate_toilets_count', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}<div class=\"input-row-content\"><div class=\"styled-radio\">\n{input}\n{error}</div></div>",
    ])->radioList(ObjectDirectory::getSeparateToiletNumberList(), [
        'tag'  => false,
        'item' => function ($index, $label, $name, $checked, $value) {
            $id = "{$name}_{$index}";
            $input = Html::input('radio', $name, $value, ['id' => $id, 'checked' => $checked]);
            $label = Html::label($label, $id);

            return Html::label($input . $label, null, ['class' => 'styled-radio__item']);
        },
    ]) ?>

    <?= $form->field($model, 'combined_toilets_count', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}<div class=\"input-row-content\"><div class=\"styled-radio\">\n{input}\n{error}</div></div>",
    ])->radioList(ObjectDirectory::getCombinedToiletNumberList(), [
        'tag'  => false,
        'item' => function ($index, $label, $name, $checked, $value) {
            $id = "{$name}_{$index}";
            $input = Html::input('radio', $name, $value, ['id' => $id, 'checked' => $checked]);
            $label = Html::label($label, $id);

            return Html::label($input . $label, null, ['class' => 'styled-radio__item']);
        },
    ]) ?>

    <?= $form->field($model, 'repairs', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}<div class=\"input-row-content\"><div class=\"styled-radio\">\n{input}\n{error}</div></div>",
    ])->radioList(ObjectDirectory::getRepairTypeMap(), [
        'tag'  => false,
        'item' => function ($index, $label, $name, $checked, $value) {
            $id = "{$name}_{$index}";
            $input = Html::input('radio', $name, $value, ['id' => $id, 'checked' => $checked]);
            $label = Html::label($label, $id);

            return Html::label($input . $label, null, ['class' => 'styled-radio__item']);
        },
    ]) ?>

    <?= $form->field($model, 'has_phone', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}<div class=\"input-row-content\"><div class=\"styled-radio\">\n{input}\n{error}</div></div>",
    ])->radioList(ObjectDirectory::getPhoneVariantList(), [
        'tag'  => false,
        'item' => function ($index, $label, $name, $checked, $value) {
            $id = "{$name}_{$index}";
            $input = Html::input('radio', $name, $value, ['id' => $id, 'checked' => $checked]);
            $label = Html::label($label, $id);

            return Html::label($input . $label, null, ['class' => 'styled-radio__item']);
        },
    ]) ?>

    <div class="input-row">
        <label></label>
        <div class="input-row-content" style="text-align: right">
            <a class="button disabled" style="margin-right: 15px;">Назад</a>
            <?= Html::submitButton('Далее', ['class' => 'button']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
