<?php
/* @var $this \yii\web\View */
/* @var $model \backend\modules\object\models\forms\DescriptionForm*/

use backend\modules\object\models\StepService;
use common\modules\photo\models\PhotoCategory;
use common\modules\photo\models\PhotoType;
use common\modules\photo\widget\dropzone\Dropzone;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div data-wizard-id="2" class="input-wizard__content__item active-wizard">
    <?php $form = ActiveForm::begin() ?>
    <div class="input-row">
        <p>Не допускаются к размещению фотографии с водяными знаками, чужих объектов и рекламные баннеры. JPG, PNG или
            GIF. Максимальный размер файла 10 мб</p>
    </div>
    <?= Dropzone::widget([
        'type'          => PhotoType::OBJECT,
        'objectId'      => $model->getObjectId(),
        'category'      => PhotoCategory::PHOTO,
        'placeholder'   => 'Загрузите фото объекта',
        'models'        => $model->getObject()->photos,
        'fileInputName' => 'Photo[small]',
    ]) ?>

    <?= Dropzone::widget([
        'type'          => PhotoType::OBJECT,
        'objectId'      => $model->getObjectId(),
        'category'      => PhotoCategory::PLAN,
        'placeholder'   => 'Загрузите план объекта',
        'models'        => $model->getObject()->plan,
        'fileInputName' => 'Photo[small]',
    ]) ?>

    <?= Dropzone::widget([
        'type'          => PhotoType::OBJECT,
        'objectId'      => $model->getObjectId(),
        'category'      => PhotoCategory::BUILDING,
        'placeholder'   => 'Загрузите фото дома',
        'models'        => $model->getObject()->buildingPhotos,
        'fileInputName' => 'Photo[small]',
    ]) ?>

    <h2>Описание объекта</h2>
    <?= $form->field($model, 'description', [
        'options'  => ['class' => 'input-row'],
        'template' => "{input}\n{error}",
    ])->textarea(['style' => 'width: 100%', 'rows' => 3]) ?>
    <div class="input-row">
        <label></label>
        <div class="input-row-content" style="text-align: right">
            <?= Html::a('Назад', ['update', 'id' => $model->getObjectId(), 'step' => StepService::BUILDING_STEP],
                ['class' => 'button', 'style' => 'margin-right: 15px;']) ?>
            <?= Html::submitButton('Далее', ['class' => 'button']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>