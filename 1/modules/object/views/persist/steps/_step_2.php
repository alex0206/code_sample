<?php
/* @var $this \yii\web\View */
/* @var $model \backend\modules\object\models\forms\BuildingForm */

use backend\modules\object\models\StepService;
use common\helpers\DateTimeHelper;
use common\models\object\ObjectDirectory;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div data-wizard-id="1" class="input-wizard__content__item active-wizard">
    <?php $form = ActiveForm::begin() ?>

    <?= $form->field($model, 'management_company_name', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}<div class=\"input-row-content\">{input}\n{error}</div>",
    ])->textInput(['placeholder' => 'Например, ЖК “Дубрава”']) ?>

    <?= $form->field($model, 'build_year', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}\n<div class=\"input-row-content\">" . "<div class=\"inline-blocks\">" .
            "<div class=\"inline-blocks__item mini-item\">{input}</div>\n{error}</div></div>",
    ])->dropDownList(DateTimeHelper::getYearList()) ?>

    <div class="input-row">
        <label>Тип и серия дома</label>
        <div class="input-row-content">
            <div class="inline-blocks">
                <?= $form->field($model, 'build_type', [
                    'options'  => ['class' => 'inline-blocks__item mini-item'],
                    'template' => "{input}\n{error}",
                ])->dropDownList(ObjectDirectory::getBuildingTypeMap()) ?>

                <?= $form->field($model, 'build_serial', [
                    'options'  => ['class' => 'inline-blocks__item mini-item'],
                    'template' => "{input}\n{error}",
                ])->textInput() ?>
            </div>
        </div>
    </div>
    <div class="input-row">
        <?= Html::activeLabel($model, 'ceiling_height') ?>
        <div class="input-row-content">
            <div class="inline-blocks">
                <?= $form->field($model, 'ceiling_height', [
                    'options'  => ['class' => 'inline-blocks__item mini-item'],
                    'template' => "{input}\n{error}",
                ])->textInput(['placeholder' => '0']) ?>
                <div class="inline-blocks__item">
                    м
                </div>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'passenger_lift_count', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}<div class=\"input-row-content\"><div class=\"styled-radio\">\n{input}\n{error}</div></div>",
    ])->radioList(ObjectDirectory::getLiftCountNumberList(), [
        'tag'  => false,
        'item' => function ($index, $label, $name, $checked, $value) {
            $id = "{$name}_{$index}";
            $input = Html::input('radio', $name, $value, ['id' => $id, 'checked' => $checked]);
            $label = Html::label($label, $id);

            return Html::label($input . $label, null, ['class' => 'styled-radio__item']);
        },
    ]) ?>

    <?= $form->field($model, 'service_lift_count', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}<div class=\"input-row-content\"><div class=\"styled-radio\">\n{input}\n{error}</div></div>",
    ])->radioList(ObjectDirectory::getLiftCountNumberList(), [
        'tag'  => false,
        'item' => function ($index, $label, $name, $checked, $value) {
            $id = "{$name}_{$index}";
            $input = Html::input('radio', $name, $value, ['id' => $id, 'checked' => $checked]);
            $label = Html::label($label, $id);

            return Html::label($input . $label, null, ['class' => 'styled-radio__item']);
        },
    ]) ?>

    <?= $form->field($model, 'parking', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}<div class=\"input-row-content\"><div class=\"styled-radio\">\n{input}\n{error}</div></div>",
    ])->radioList(ObjectDirectory::getParkingTypeMap(), [
        'tag'  => false,
        'item' => function ($index, $label, $name, $checked, $value) {
            $id = "{$name}_{$index}";
            $input = Html::input('radio', $name, $value, ['id' => $id, 'checked' => $checked]);
            $label = Html::label($label, $id);

            return Html::label($input . $label, null, ['class' => 'styled-radio__item']);
        },
    ]) ?>

    <div class="input-row">
        <label>Дополнительно</label>
        <div class="input-row-content">
            <div class="inline-blocks">
                <?= $form->field($model, 'has_garbage_chute', [
                    'options'  => ['class' => 'inline-blocks__item'],
                    'template' => "{input}\n{error}",
                ])->checkbox() ?>
            </div>
        </div>
    </div>
    <div class="input-row">
        <label></label>
        <div class="input-row-content" style="text-align: right">
            <?= Html::a('Назад', ['update', 'id' => $model->getObjectId(), 'step' => StepService::OBJECT_STEP],
                ['class' => 'button', 'style' => 'margin-right: 15px;']) ?>
            <?= Html::submitButton('Далее', ['class' => 'button']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>