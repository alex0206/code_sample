<?php
/* @var $this \yii\web\View */
/* @var $model \backend\modules\object\models\forms\ClientForm*/

use backend\modules\object\models\StepService;
use common\models\object\ObjectDirectory;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

?>
<div data-wizard-id="4" class="input-wizard__content__item active-wizard">
    <?php $form = ActiveForm::begin() ?>

    <?= $form->field($model, 'client_type', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}\n<div class=\"input-row-content\">" . "<div class=\"inline-blocks\">" .
            "<div class=\"inline-blocks__item mini-item\">{input}</div>\n{error}</div></div>",
    ])->dropDownList(ObjectDirectory::getClientTypeMap()) ?>

    <?= $form->field($model, 'status', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}\n<div class=\"input-row-content\">" . "<div class=\"inline-blocks\">" .
            "<div class=\"inline-blocks__item mini-item\">{input}</div>\n{error}</div></div>",
    ])->dropDownList(ObjectDirectory::getStatusTypeMap()) ?>

    <?= $form->field($model, 'client_name', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}\n<div class=\"input-row-content\">{input}</div>\n{error}",
    ])->textInput() ?>

    <?= $form->field($model, 'client_phone', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}\n<div class=\"input-row-content\">{input}</div>\n{error}",
    ])
        ->widget(MaskedInput::className(), ['mask' => '+9 (999) 999 99 99',])
    ?>

    <?= $form->field($model, 'client_email', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}\n<div class=\"input-row-content\">{input}</div>\n{error}",
    ])->textInput() ?>

    <?= $form->field($model, 'client_birthday', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}\n<div class=\"input-row-content\">{input}</div>\n{error}",
    ])->textInput(['class' => 'js-datepicker']) ?>

    <div class="input-row">
        <label></label>
        <div class="input-row-content" style="text-align: right">
            <?= Html::a('Назад', ['update', 'id' => $model->getObjectId(), 'step' => StepService::PRICE_STEP],
                ['class' => 'button', 'style' => 'margin-right: 15px;']) ?>
            <?= Html::submitButton('Сохранить', ['class' => 'button']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>