<?php
/* @var $this \yii\web\View */
/* @var $model \backend\modules\object\models\forms\PriceForm */

use backend\modules\object\models\StepService;
use common\models\object\ObjectDirectory;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div data-wizard-id="3" class="input-wizard__content__item active-wizard">
    <?php $form = ActiveForm::begin() ?>
    <div class="input-row">
        <?= Html::activeLabel($model, 'price') ?>
        <div class="input-row-content">
            <div class="inline-blocks">
                <?= $form->field($model, 'price', [
                    'options'  => ['class' => 'inline-blocks__item mini-item'],
                    'template' => "{input}\n{error}",
                ])->textInput(['class' => 'numeric', 'placeholder' => '0']) ?>
                <div class="inline-blocks__item">
                    руб.
                </div>
                <?= $form->field($model, 'possible_hypothec', [
                    'options'  => ['class' => 'inline-blocks__item'],
                    'template' => "{input}\n{error}",
                ])->checkbox() ?>
            </div>
        </div>
    </div>
    <?= $form->field($model, 'selling_type', [
        'options'  => ['class' => 'input-row'],
        'template' => "{label}\n<div class=\"input-row-content\">" . "<div class=\"inline-blocks\">" .
            "<div class=\"inline-blocks__item mini-item\">{input}</div>\n{error}</div></div>",
    ])->dropDownList(ObjectDirectory::getSellingTypeMap()) ?>

    <h2>Работа с посредниками</h2>
    <div class="input-row">
        <label>Разрешить</label>
        <div class="input-row-content">
            <div class="inline-blocks">
                <div class="inline-blocks__item">
                    <?= $form->field($model, 'enable_realtor_work', [
                        'options'  => ['tag' => false],
                        'template' => "{input}",
                    ])->checkbox(['class' => 'styled', 'id' => 'ch12'], false) ?>
                    <label for="ch12">
                        <div class="yes">да</div>
                        <div class="no">нет</div>
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="input-row">
        <label>Моя комиссия</label>
        <div class="input-row-content">
            <div class="inline-blocks">
                <?= $form->field($model, 'realtor_commission', [
                    'options'  => ['class' => 'inline-blocks__item mini-item'],
                    'template' => "{input}\n{error}",
                ])->textInput(['class' => 'numeric', 'placeholder' => '0']) ?>

                <?= $form->field($model, 'realtor_commission_type', [
                    'options'  => ['class' => 'inline-blocks__item mini-item'],
                    'template' => "{input}",
                ])->dropDownList(ObjectDirectory::getCommissionTypeMap()) ?>
            </div>
        </div>
    </div>
    <div class="input-row">
        <label></label>
        <div class="input-row-content" style="text-align: right">
            <?= Html::a('Назад', ['update', 'id' => $model->getObjectId(), 'step' => StepService::DECRIPTION_STEP],
                ['class' => 'button', 'style' => 'margin-right: 15px;']) ?>
            <?= Html::submitButton('Далее', ['class' => 'button']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
