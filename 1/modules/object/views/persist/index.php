<?php
/* @var $this \yii\web\View */
/* @var $model \backend\modules\object\models\forms\StepForm */
/* @var $step integer */

use backend\modules\object\models\StepService;

$this->title = 'Добавить агентство недвижимости';
$step = $model->getStep();
?>
<h1><?= $this->title ?></h1>
<section class="island">
    <div class="input-wizard">
        <div class="input-wizard__header col-5">
            <div data-wizard-id="0"
                 class="input-wizard__header__item<?php if ($step == StepService::OBJECT_STEP): ?> active-wizard<?php endif ?>">
                <div>
                    <b>1.</b>
                    <span>Объект</span>
                </div>
            </div>
            <div data-wizard-id="1"
                 class="input-wizard__header__item<?php if ($step == StepService::BUILDING_STEP): ?> active-wizard<?php endif ?>">
                <div>
                    <b>2.</b>
                    <span>Здание</span>
                </div>
            </div>
            <div data-wizard-id="2"
                 class="input-wizard__header__item<?php if ($step == StepService::DECRIPTION_STEP): ?> active-wizard<?php endif ?>">
                <div>
                    <b>3.</b>
                    <span>Фото и описание</span>
                </div>
            </div>
            <div data-wizard-id="3"
                 class="input-wizard__header__item<?php if ($step == StepService::PRICE_STEP): ?> active-wizard<?php endif ?>">
                <div>
                    <b>4.</b>
                    <span>Цена</span>
                </div>
            </div>
            <div data-wizard-id="4"
                 class="input-wizard__header__item<?php if ($step == StepService::CLIENT_STEP): ?> active-wizard<?php endif ?>">
                <div>
                    <b>5.</b>
                    <span>Клиент</span>
                </div>
            </div>
        </div>
        <div class="input-wizard__content">
            <?= $this->render("steps/_step_$step", ['model' => $model]) ?>
        </div>
    </div>
</section>