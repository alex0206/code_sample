<?php
/* @var $this \yii\web\View */
/* @var $model \common\models\object\ObjectEvent */

use yii\helpers\Html;

$formater = Yii::$app->formatter;
?>
<li class="object-view__notifications-list__item<?php if ($model->isFuture() && !$model->result): ?> answer-in-notification<?php endif ?>">
    <div class="date">
        <?= $formater->asDate($model->time, 'php: d.m.Y') ?><br>в <?= $formater->asDate($model->time, 'php: H:i') ?>
    </div>
    <div class="cont">
        <b><?= $model->event->name ?></b>
        <p><?= Html::encode($model->message) ?></p>

        <?php if ($model->result): ?>
            <ul>
                <li class="object-view__notifications-list__item">
                    <div class="date">
                        <?= $formater->asDate($model->result->time, 'php: d.m.Y') ?>
                        <br>в <?= $formater->asDate($model->result->time, 'php: H:i') ?>
                    </div>
                    <div class="cont">
                        <b>Результат</b>
                        <p><?= Html::encode($model->result->message) ?></p>
                    </div>
                </li>
            </ul>
        <?php elseif ($model->isFuture()): ?>
            <div class="answer">
                <div class="input-row">
                    <div class="input-row__x-mode">
                        <div class="right">
                            <?= Html::a('Добавить', ['/object/event/add-result', 'event_id' => $model->id],
                                ['data-pjax' => 0, 'class' => 'button']) ?>
                        </div>
                        <div class="left">
                            <input type="text" placeholder="Результат">
                        </div>
                    </div>
                </div>
            </div>
        <?php endif ?>
    </div>
</li>