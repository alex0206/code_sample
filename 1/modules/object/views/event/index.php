<?php
/* @var $this \yii\web\View */
/* @var $dataEventProvider \yii\data\ActiveDataProvider */
/* @var $eventForm \backend\modules\object\models\forms\EventForm */
/* @var $object_id int */


use backend\modules\object\assets\event\EventAsset;
use yii\widgets\ListView;
use yii\widgets\Pjax;

EventAsset::register($this);

Pjax::begin(['enablePushState' => false, 'id' => 'object_event_pjax']) ?>
<?= $this->render('_form', compact('eventForm', 'object_id')) ?>
    <hr>
<?= ListView::widget([
    'dataProvider' => $dataEventProvider,
    'options'      => ['tag' => 'ul', 'class' => 'object-view__notifications-list'],
    'itemView'     => '_event',
    'itemOptions'  => ['tag' => false],
    'layout'       => "{items}",
]); ?>
<?php Pjax::end() ?>