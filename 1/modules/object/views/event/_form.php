<?php
/* @var $this \yii\web\View */
/* @var $eventForm \backend\modules\object\models\forms\EventForm */
/* @var $object_id int */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin([
    'options' => ['data-pjax' => 1],
    'action'  => ['/object/event/list', 'object_id' => $object_id],
]) ?>
    <div class="input-row object-view-actions">
        <div class="inline-blocks">
            <div class="inline-blocks__item mini-item">
                <?= $form->field($eventForm, 'event_id',
                    ['template' => '{input}'])->dropDownList($eventForm->getEventMap()) ?>
            </div>
            <div class="inline-blocks__item mini-item">
                <?php //todo Добавить datepicker?>
                <a class="time-selector"><i></i><span>Запланировать<br> на время</span></a>
                <?= $form->field($eventForm, 'time',
                    ['template' => '{input}'])->textInput(['placeholder' => 'день.месяц.год час:мин']) ?>
            </div>
            <div class="inline-blocks__item mini-item">
                <div class="notifications">
                    Уведомить клиента<br>
                    <?= $form->field($eventForm, 'sms_notice',
                        ['template' => '{input}', 'options' => ['tag' => false]])->checkbox() ?>
                    <?= $form->field($eventForm, 'email_notice',
                        ['template' => '{input}', 'options' => ['tag' => false]])->checkbox() ?>
                </div>
            </div>
        </div>
    </div>
    <div class="input-row">
        <div class="input-row__x-mode">
            <div class="right">
                <?= Html::submitButton('Добавить', ['class' => 'button']) ?>
            </div>
            <div class="left">
                <?= $form->field($eventForm, 'message',
                    ['template' => "{input}\n{error}"])->textInput(['placeholder' => $eventForm->getAttributeLabel('message')]) ?>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>