<?php


namespace backend\modules\object\controllers;


use backend\modules\object\models\forms\EventForm;
use backend\modules\object\models\ObjectEventService;
use common\behaviors\AjaxResponse;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class EventController extends Controller
{
    private $eventService;

    public function __construct($id, $module, ObjectEventService $eventService, array $config = [])
    {
        $this->eventService = $eventService;
        parent::__construct($id, $module, $config);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access'       => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'ajaxResponse' => [
                'class'   => AjaxResponse::className(),
                'actions' => ['add-result'],
            ],
        ];
    }

    public function actionAddResult($event_id)
    {
        $message = Yii::$app->request->post('message');

        if ($message) {
            $this->eventService->addResult($event_id, $message);

            return 1;
        }

        return 0;
    }

    public function actionList($object_id)
    {
        $eventForm = new EventForm();

        if ($eventForm->load(Yii::$app->request->post()) && $eventForm->validate()) {
            $eventForm->object_id = $object_id;
            $this->eventService->addEvent($eventForm);
            $eventForm->reset();
        }

        $dataEventProvider = $this->eventService->getDataProviderByObject($object_id);

        return $this->render('index', compact('dataEventProvider', 'eventForm', 'object_id'));
    }
}