<?php


namespace backend\modules\object\controllers;


use backend\modules\object\models\forms\EventForm;
use backend\modules\object\models\ObjectEventService;
use backend\modules\object\models\ObjectSearch;
use backend\modules\object\models\ObjectService;
use common\exceptions\NotFoundException;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ListController extends Controller
{
    private $objectService;
    private $eventService;

    public function __construct(
        $id,
        $module,
        ObjectService $objectService,
        ObjectEventService $eventService,
        array $config = []
    ) {
        $this->objectService = $objectService;
        $this->eventService = $eventService;

        parent::__construct($id, $module, $config);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ObjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $isSerching = !empty(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel', 'dataProvider', 'isSerching'));
    }

    /**
     * @param int $id Object id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView(int $id)
    {
        try {
            $model = $this->objectService->getRepository()->findOne($id);
            $dataEventProvider = $this->eventService->getDataProviderByObject($id);
            $eventForm = new EventForm();
        } catch (NotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage());
        }

        return $this->render('view', compact('model', 'dataEventProvider', 'eventForm'));
    }
}