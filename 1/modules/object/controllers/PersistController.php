<?php

namespace backend\modules\object\controllers;

use backend\modules\object\models\ObjectService;
use backend\modules\object\models\StepService;
use common\exceptions\NotFoundException;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Persist controller for the `object` module
 */
class PersistController extends Controller
{
    private $objectService;
    private $stepService;

    public function __construct(
        $id,
        $module,
        ObjectService $objectService,
        StepService $stepService,
        array $config = []
    ) {
        $this->objectService = $objectService;
        $this->stepService = $stepService;

        parent::__construct($id, $module, $config);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $form = $this->objectService->getModelForm(StepService::OBJECT_STEP);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->objectService->save($form);

            return $this->redirect(['update', 'id' => $form->getObjectId(), 'step' => StepService::BUILDING_STEP], 200);
        }

        return $this->render('index', ['model' => $form]);
    }

    /**
     * @param int $id Object id
     * @param int $step
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate(int $id, int $step = StepService::OBJECT_STEP)
    {
        try {
            $form = $this->objectService->getModelForm($step, $id);
        } catch (NotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage());
        }

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->objectService->save($form);

            if ($step == StepService::CLIENT_STEP) {
                return $this->redirect(['/object/list/view/', 'id' => $id]);
            }

            return $this->redirect(['update', 'id' => $id, 'step' => $this->stepService->getNextStep($step)], 200);
        }

        return $this->render('index', ['model' => $form]);
    }
}