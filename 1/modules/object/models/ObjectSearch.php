<?php


namespace backend\modules\object\models;

use common\models\object\ObjectDirectory;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

class ObjectSearch extends Model
{
    const PAGE_SIZE = 2;

    public $type;
    public $room_number;
    public $min_price;
    public $max_price;
    public $search;
    public $sort;
    public $city;
    public $offer_type;

    public function init()
    {
        $this->type = current($this->getTypeList());
        $this->offer_type = current($this->getOfferTypeList());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['type', 'in', 'range' => $this->getTypeList()],
            ['offer_type', 'in', 'range' => $this->getOfferTypeList()],
            [['min_price', 'max_price'], 'number'],
            [
                ['min_price', 'max_price'],
                'filter',
                'filter' => function ($value) {
                    return $value ? $this->convertPriceToMinValue($value) : null;
                },
            ],
            [['search'], 'string'],
            [['room_number'], 'safe'],
            ['city', 'default', 'value' => 'Ижевск'],
        ];
    }

    public function getTypeList()
    {
        return ObjectDirectory::getTypeList();
    }

    public function getOfferTypeList()
    {
        return ObjectDirectory::getOfferTypeList();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Object::find()
            ->with([
                'createdBy' => function (ActiveQuery $quey) {
                    $quey->select(['user_id', 'name'])->with([
                        'company' => function (ActiveQuery $query) {
                            $query->select(['company_id', 'brand_name']);
                        },
                    ]);
                },
                'photos',
                'plan',
                'buildingPhotos',
            ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => [
                'defaultOrder' => [
                    'price' => SORT_ASC,
                ],
                'attributes'   => [
                    'price',
                ],
            ],
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');

            return $dataProvider;
        }

        $query->where([
            'city'       => $this->city,
            'step'       => StepService::CLIENT_STEP,
            'type'       => $this->type,
            'offer_type' => $this->offer_type,
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            'or',
            ['like', 'id', $this->search],
            ['like', 'apt_number', $this->search],
            ['like', 'total_square', $this->search],
            ['like', 'room_square', $this->search],
            ['like', 'floor', $this->search],
            ['like', 'max_floor', $this->search],
            ['like', 'kitchen_square', $this->search],
            ['like', 'loggia_number', $this->search],
            ['like', 'balcony_number', $this->search],
            ['like', 'separate_toilets_count', $this->search],
            ['like', 'combined_toilets_count', $this->search],
            ['like', 'build_year', $this->search],
            ['like', 'ceiling_height', $this->search],
            ['like', 'passenger_lift_count', $this->search],
            ['like', 'service_lift_count', $this->search],
            ['like', 'street', $this->search],
            ['like', 'management_company_name', $this->search],
            ['like', 'description', $this->search],
        ])
            ->andFilterWhere(['<=', 'price', $this->max_price])
            ->andFilterWhere(['>=', 'price', $this->min_price])
            ->andFilterWhere(['room_number' => $this->room_number]);

        if ($this->max_price) {
            $this->max_price = $this->convertPriceToMaxValue($this->max_price);
        }

        if ($this->min_price) {
            $this->min_price = $this->convertPriceToMaxValue($this->min_price);
        }

        return $dataProvider;
    }

    public function load($data, $formName = null)
    {
        $this->sort = Yii::$app->request->get('sort');

        return parent::load($data, $formName);
    }

    /**
     * Convert to kopeks
     * @param float $value
     * @return int
     */
    private function convertPriceToMinValue(float $value): int
    {
        return Yii::$app->money->valueToMin($value);
    }

    /**
     * Convert to rubles
     * @param int $value
     * @return float
     */
    private function convertPriceToMaxValue(int $value): float
    {
        return Yii::$app->money->valueToMax($value);
    }
}