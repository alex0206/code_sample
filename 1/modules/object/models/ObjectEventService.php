<?php


namespace backend\modules\object\models;


use backend\modules\object\models\forms\EventForm;
use backend\modules\object\repositories\EventRepository;
use backend\modules\object\repositories\EventResultRepository;
use backend\modules\object\repositories\ObjectRepository;
use common\components\EmailSender;
use Yii;
use yii\data\ActiveDataProvider;

class ObjectEventService
{
    private $eventRepository;
    private $eventResultRepository;
    private $objectRepository;

    public function __construct(
        EventRepository $eventRepository,
        EventResultRepository $eventResultRepository,
        ObjectRepository $objectRepository
    ) {
        $this->eventRepository = $eventRepository;
        $this->eventResultRepository = $eventResultRepository;
        $this->objectRepository = $objectRepository;
    }

    public function getDataProviderByObject(int $objectId): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query'      => ObjectEvent::find()
                ->where(['object_id' => $objectId])
                ->with(['event', 'result'])
                ->orderBy(['time' => SORT_DESC]),
            'pagination' => [
                'pageSize' => null,
            ],
        ]);
    }

    public function addEvent(EventForm $dataForm)
    {
        $eventModel = new ObjectEvent();
        $eventModel->load($dataForm->getAttributes(), '');

        $this->eventRepository->save($eventModel);

        if ($eventModel->email_notice || $eventModel->sms_notice) {
            $clientInfo = $this->objectRepository->getClientInfo($eventModel->object_id);

            if ($eventModel->email_notice && $clientInfo['client_email']) {
                (new EmailSender())->send('event', $clientInfo['client_email'], [
                    'name'    => $clientInfo['client_name'],
                    'message' => $eventModel->message,
                ], 'Событие');
            }

            if ($eventModel->sms_notice && $clientInfo['client_phone']) {
                $this->sendSms($clientInfo['client_phone'], $eventModel->message);
            }
        }
    }

    public function addResult(int $eventId, string $message)
    {
        $model = new ObjectEventResult(['event_id' => $eventId, 'message' => $message]);

        $this->eventResultRepository->save($model);
    }

    /**
     * @param string $phone
     * @param string $message
     * @return mixed
     */
    protected function sendSms($phone, $message)
    {
        return Yii::$app->sms->send($phone, $message);
    }
}