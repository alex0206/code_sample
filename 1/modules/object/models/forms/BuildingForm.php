<?php


namespace backend\modules\object\models\forms;


use backend\modules\object\models\StepFactory;
use backend\modules\object\models\StepService;
use common\models\object\Object;
use common\models\object\ObjectDirectory;

class BuildingForm extends StepForm
{
    public $management_company_name;
    public $build_year;
    public $build_type;
    public $build_serial;
    public $ceiling_height;
    public $passenger_lift_count;
    public $service_lift_count;
    public $parking;
    public $has_garbage_chute;

    public function init()
    {
        $this->passenger_lift_count = 0;
        $this->service_lift_count = 0;
        $this->parking = Object::PARKING_GROUND;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['has_garbage_chute',], 'boolean'],
            [
                [
                    'passenger_lift_count',
                    'service_lift_count',
                ],
                'integer',
            ],
            [['ceiling_height'], 'number'],
            [
                [
                    'management_company_name',
                    'build_type',
                    'build_serial',
                    'parking',
                ],
                'string',
                'max' => 255,
            ],
            ['build_year', 'date', 'format' => 'Y'],
            ['build_type', 'in', 'range' => ObjectDirectory::getBuildingTypeList()],
            ['parking', 'in', 'range' => ObjectDirectory::getParkingVariantList()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'management_company_name' => 'Название ЖК',
            'build_year'              => 'Год постройки',
            'build_type'              => 'Тип дома',
            'build_serial'            => 'Серия дома',
            'ceiling_height'          => 'Высота потолков',
            'passenger_lift_count'    => 'Пассажирских лифтов',
            'service_lift_count'      => 'Грузовых лифтов',
            'parking'                 => 'Парковка',
            'has_garbage_chute'       => 'Мусоропровод в доме',
        ];
    }

    public function getStep(): int
    {
        return StepService::BUILDING_STEP;
    }
}