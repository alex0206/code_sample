<?php


namespace backend\modules\object\models\forms;


use backend\modules\object\models\StepService;
use common\models\object\Object;
use common\models\object\ObjectDirectory;

class ObjectForm extends StepForm
{
    public $offer_type;
    public $ownership_type;
    public $type;
    public $street;
    public $city;
    public $build_number;
    public $housing;
    public $apt_number;
    public $cadastral_number;
    public $room_number;
    public $is_apartment;
    public $is_penthouse;
    public $total_square;
    public $floor;
    public $max_floor;
    public $room_square;
    public $kitchen_square;
    public $loggia_number;
    public $balcony_number;
    public $is_yard_window;
    public $is_street_window;
    public $separate_toilets_count;
    public $combined_toilets_count;
    public $repairs;
    public $has_phone;

    public function init()
    {
        $this->offer_type = Object::OFFER_TYPE_SELLING;
        $this->ownership_type = Object::OWNERSHIP_TYPE_RESIDENTIAL;
        $this->loggia_number = 0;
        $this->balcony_number = 0;
        $this->separate_toilets_count = 0;
        $this->combined_toilets_count = 0;
        $this->repairs = Object::REPAIR_COSMETIC;
        $this->has_phone = 0;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'street',
                    'city',
                    'build_number',
                    'apt_number',
                    'cadastral_number',
                    'room_number',
                    'total_square',
                    'floor',
                    'max_floor',
                    'room_square',
                ],
                'required',
            ],
            [['is_apartment', 'is_penthouse', 'is_yard_window', 'is_street_window', 'has_phone',], 'boolean'],
            [['total_square', 'kitchen_square', 'room_square'], 'number'],
            [
                [
                    'room_number',
                    'floor',
                    'max_floor',
                    'loggia_number',
                    'balcony_number',
                    'separate_toilets_count',
                    'combined_toilets_count',
                    'apt_number',
                ],
                'integer',
            ],
            [
                [
                    'offer_type',
                    'ownership_type',
                    'type',
                    'cadastral_number',
                    'street',
                    'city',
                    'build_number',
                    'housing',
                ],
                'string',
                'max' => 255,
            ],
            ['offer_type', 'in', 'range' => ObjectDirectory::getOfferTypeList()],
            ['ownership_type', 'in', 'range' => ObjectDirectory::getOwnershipTypeList()],
            ['repairs', 'in', 'range' => ObjectDirectory::getRepairVariantList()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'offer_type'             => 'Тип предложения',
            'ownership_type'         => 'Тип собственности',
            'type'                   => 'Тип',
            'cadastral_number'       => 'Кадастровый номер',
            'room_number'            => 'Количество комнат',
            'is_apartment'           => 'Аппартаменты',
            'is_penthouse'           => 'Пентхаус',
            'total_square'           => 'Общая площадь',
            'floor'                  => 'Этаж',
            'max_floor'              => 'Количество этажей',
            'room_square'            => 'Жилая площадь',
            'kitchen_square'         => 'Кухня',
            'loggia_number'          => 'Лоджия',
            'balcony_number'         => 'Балкон',
            'is_yard_window'         => 'Во двор',
            'is_street_window'       => 'На улицу',
            'separate_toilets_count' => 'Раздельные санузлы',
            'combined_toilets_count' => 'Совмещенные санузлы',
            'repairs'                => 'Ремонт',
            'has_phone'              => 'Телефон',
            'street'                 => 'Адрес объекта',
            'city'                   => 'Город объекта',
            'build_number'           => 'Дом',
            'housing'                => 'Корпус',
            'apt_number'             => 'Квартира',

        ];
    }

    public function getStep(): int
    {
        return StepService::OBJECT_STEP;
    }
}