<?php


namespace backend\modules\object\models\forms;


use backend\modules\object\models\StepService;
use common\helpers\PhoneHelper;

class ClientForm extends StepForm
{
    public $client_type;
    public $status;
    public $client_name;
    public $client_phone;
    public $client_email;
    public $client_birthday;

    public function rules()
    {
        return [
            [
                ['client_phone'],
                'filter',
                'filter' => function ($value) {
                    return PhoneHelper::filter($value);
                },
            ],
            [['client_name', 'client_phone', 'client_email', 'client_birthday'], 'required'],
            [
                'client_birthday',
                'filter',
                'filter' => function ($value) {
                    return date('Y-m-d', strtotime($value));
                },
            ],
            [['client_birthday'], 'date', 'format' => 'Y-m-d'],
            [
                [
                    'client_type',
                    'status',
                    'client_name',
                    'client_phone',
                ],
                'string',
                'max' => 255,
            ],
            ['client_email', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client_type'     => 'Тип клиента',
            'status'          => 'Статус',
            'client_name'     => 'ФИО',
            'client_phone'    => 'Телефон',
            'client_email'    => 'Эл. почта',
            'client_birthday' => 'Дата рождения',
        ];
    }

    public function getStep(): int
    {
        return StepService::CLIENT_STEP;
    }
}