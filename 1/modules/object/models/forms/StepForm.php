<?php


namespace backend\modules\object\models\forms;


use backend\modules\object\models\Object as OjectModel;
use yii\base\Model;

abstract class StepForm extends Model
{
    private $object;

    public function getObjectId()
    {
        return $this->object->id ?? null;
    }

    public function setObject(OjectModel $object)
    {
        $this->object = $object;
    }

    public function getObject(): OjectModel
    {
        return $this->object;
    }

    abstract public function getStep(): int;
}