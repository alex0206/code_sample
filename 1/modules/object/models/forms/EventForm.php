<?php


namespace backend\modules\object\models\forms;


use backend\modules\object\repositories\EventListRepository;
use common\helpers\ArrayHelper;
use yii\base\Model;

class EventForm extends Model
{
    public $event_id;
    public $message;
    public $time;
    public $email_notice;
    public $sms_notice;
    public $object_id;

    /** @var  EventListRepository */
    private $eventListRepository;

    public function init()
    {
        $this->eventListRepository = new EventListRepository();
    }

    public function rules()
    {
        return [
            [['event_id', 'message'], 'required'],
            [['email_notice', 'sms_notice'], 'boolean'],
            [
                'time',
                'filter',
                'filter' => function ($value) {
                    if ($value) {
                        $result = strtotime($value);

                        if ($result > time()) {
                            return $result;
                        }
                    }

                    return time();
                },
            ],
            [['message'], 'string', 'max' => 255],
            ['event_id', 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'message'      => 'Сообщение',
            'time'         => 'Запланировать на время',
            'email_notice' => 'Эл. почта',
            'sms_notice'   => 'SMS',
        ];
    }

    public function getEventMap(): array
    {
        return ArrayHelper::map($this->eventListRepository->getAll(true), 'id', 'name');
    }

    public function reset()
    {
        $attributes = $this->getAttributes();

        foreach ($attributes as $key => $value) {
            $this->$key = null;
        }
    }
}