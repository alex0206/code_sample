<?php


namespace backend\modules\object\models\forms;


use backend\modules\object\models\StepService;

class DescriptionForm extends StepForm
{
    public $description;

    public function rules()
    {
        return [
            [['description'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'description' => 'Описание',
        ];
    }

    public function getStep(): int
    {
        return StepService::DECRIPTION_STEP;
    }
}