<?php


namespace backend\modules\object\models\forms;


use backend\modules\object\models\StepService;
use common\models\object\Object;

class PriceForm extends StepForm
{
    public $price;
    public $possible_hypothec;
    public $selling_type;
    public $enable_realtor_work;
    public $realtor_commission;
    public $realtor_commission_type;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['realtor_commission',],
                'number',
                'when'       => function (PriceForm $model) {
                    return $model->realtor_commission_type === Object::COMMISSION_TYPE_MONEY;
                },
                'whenClient' => 'function(attr,value){
                    return $("#priceform-realtor_commission_type").val() === "' . Object::COMMISSION_TYPE_MONEY . '";
                }',
            ],
            [['price',], 'number'],
            [['enable_realtor_work', 'possible_hypothec'], 'boolean'],
            [['selling_type', 'realtor_commission_type',], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'price'                   => 'Цена',
            'possible_hypothec'       => 'Возможна Ипотека',
            'selling_type'            => 'Тип продажи',
            'enable_realtor_work'     => 'Разрешить',
            'realtor_commission'      => 'Моя комиссия',
            'realtor_commission_type' => '',
        ];
    }

    public function getStep(): int
    {
        return StepService::PRICE_STEP;
    }
}