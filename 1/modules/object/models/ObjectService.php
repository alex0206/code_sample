<?php


namespace backend\modules\object\models;


use backend\modules\object\models\forms\StepForm;
use backend\modules\object\repositories\ObjectRepository;
use backend\modules\object\models\Object as ObjectModel;

class ObjectService
{
    private $objectRepository;
    private $stepService;

    public function __construct(ObjectRepository $objectRepository, StepService $stepService)
    {
        $this->objectRepository = $objectRepository;
        $this->stepService = $stepService;
    }

    /**
     * @param int $step
     * @param int|null $id Object id
     * @return StepForm
     */
    public function getModelForm(int $step, int $id = null): StepForm
    {
        $stepDeterminated = $this->stepService->stepDetermine($step, $id);
        $form = StepFactory::createStepForm($stepDeterminated);

        if ($id) {
            $object = $this->objectRepository->findOne($id);

            if ($this->isSavedStep($object, $stepDeterminated)) {
                $form->load($object->getAttributes(), '');
            }

            $form->setObject($object);
        }

        return $form;
    }

    public function save(StepForm $form)
    {
        $objectId = $form->getObjectId();

        $object = $this->getObjectModel($objectId);
        $object->load($form->getAttributes(), '');
        $object->setStep($form->getStep());

        $this->objectRepository->save($object);

        $form->setObject($object);
    }

    public function getRepository(): ObjectRepository
    {
        return $this->objectRepository;
    }

    private function getObjectModel(int $objectId = null): ObjectModel
    {
        if ($objectId) {
            $object = $this->objectRepository->findOne($objectId);
        } else {
            $object = new ObjectModel();
        }

        return $object;
    }

    /**
     * @param $object
     * @param $stepDeterminated
     * @return bool
     */
    private function isSavedStep($object, $stepDeterminated): bool
    {
        return $object->step >= $stepDeterminated;
    }
}