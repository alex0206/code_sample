<?php


namespace backend\modules\object\models;


class Object extends \common\models\object\Object
{
    public function setStep(int $step)
    {
        if ($step > $this->step) {
            $this->step = $step;
        }
    }
}