<?php


namespace backend\modules\object\models;


use backend\modules\object\repositories\ObjectRepository;
use InvalidArgumentException;

class StepService
{
    const OBJECT_STEP = 1;
    const BUILDING_STEP = 2;
    const DECRIPTION_STEP = 3;
    const PRICE_STEP = 4;
    const CLIENT_STEP = 5;

    private $step;
    private $objectRepository;

    public function __construct(ObjectRepository $objectRepository)
    {
        $this->objectRepository = $objectRepository;
    }

    public function getStep(): int
    {
        return $this->step;
    }

    public function stepDetermine(int $step, int $objectId = null): int
    {
        if (!$this->validate($step)) {
            throw new InvalidArgumentException('This step does not exist');
        }

        if (!$objectId) {
            $this->step = self::OBJECT_STEP;
        } else {
            $object = $this->objectRepository->findOne($objectId);
            $resultCompare = $step <=> $object->step;

            if ($resultCompare === -1 || $resultCompare === 0) {
                $this->step = $step;
            } else {
                $this->step = $this->getNextStep($object->step);
            }
        }

        return $this->step;
    }

    public function validate(int $step): bool
    {
        return in_array($step, $this->getStepList());
    }

    public function getStepList(): array
    {
        return [
            self::OBJECT_STEP,
            self::BUILDING_STEP,
            self::DECRIPTION_STEP,
            self::PRICE_STEP,
            self::CLIENT_STEP,
        ];
    }

    public function getNextStep(int $step): int
    {
        if ($step == $this->getMaxStep() || !$this->validate($step)) {
            return self::OBJECT_STEP;
        }

        return $step + 1;
    }

    public function getMaxStep(): int
    {
        return self::CLIENT_STEP;
    }
}