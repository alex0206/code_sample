<?php


namespace backend\modules\object\models;


use backend\modules\object\models\forms\BuildingForm;
use backend\modules\object\models\forms\ClientForm;
use backend\modules\object\models\forms\DescriptionForm;
use backend\modules\object\models\forms\ObjectForm;
use backend\modules\object\models\forms\PriceForm;
use backend\modules\object\models\forms\StepForm;
use yii\base\InvalidParamException;

class StepFactory
{
    public static function createStepForm(int $step): StepForm
    {
        switch ($step) {
            case StepService::OBJECT_STEP:
                return new ObjectForm();
            case StepService::BUILDING_STEP:
                return new BuildingForm();
            case StepService::DECRIPTION_STEP:
                return new DescriptionForm();
            case StepService::PRICE_STEP:
                return new PriceForm();
            case StepService::CLIENT_STEP:
                return new ClientForm();
            default:
                throw new InvalidParamException('Not supported step');
        }
    }
}