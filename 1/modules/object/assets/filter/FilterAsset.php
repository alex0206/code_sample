<?php

namespace backend\modules\object\assets\filter;

use yii\web\AssetBundle;

class FilterAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/object/assets/filter/source';

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public $css = [
    ];

    public $js = [
        'js/app.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}