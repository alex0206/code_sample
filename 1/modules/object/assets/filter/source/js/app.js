;(function ($) {
    'use strict';

    var app = {
        init: function () {
            this.changeEventListener();
        },
        changeEventListener: function () {
            $(document).on('change', '.object-search select, .object-search input', function () {
                $(this).parents('form').submit();
            });
        }
    };

    app.init();
})(window.jQuery);