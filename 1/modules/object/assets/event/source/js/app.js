;(function ($) {
    'use strict';

    var app = {
        init: function () {
            this.addResultEventListener();
        },
        addResultEventListener: function () {
            $(document).on('click', '.answer a', function (e) {
                e.preventDefault();
                var link = $(this);
                var resultMessage = link.parents('.input-row__x-mode').find('input').val();
                resultMessage = $.trim(resultMessage);

                if (resultMessage) {
                    $.post(link.attr('href'), {message: resultMessage}).done(function () {
                        $.pjax.reload({
                            container: '#object_event_pjax',
                            url: $('#object_event_pjax form').attr('action'),
                            push: false,
                            replace: false
                        });
                    });
                }
            });
        }
    };

    app.init();
})(window.jQuery);