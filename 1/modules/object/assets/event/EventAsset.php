<?php

namespace backend\modules\object\assets\event;

use yii\web\AssetBundle;

class EventAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/object/assets/event/source';

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public $css = [
    ];

    public $js = [
        'js/app.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}