<?php


namespace backend\modules\object\repositories;


use common\models\object\ObjectEventList;

class EventListRepository
{
    public function getAll(bool $visible = null): array
    {
        return ObjectEventList::find()->filterWhere(['visible' => $visible])->asArray()->all();
    }
}