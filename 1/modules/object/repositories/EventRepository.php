<?php


namespace backend\modules\object\repositories;


use common\models\object\ObjectEvent;
use RuntimeException;

class EventRepository
{
    /**
     * @param ObjectEvent $model
     * @throws RuntimeException
     */
    public function save(ObjectEvent $model)
    {
        if ($model->save(false) === false) {
            throw new RuntimeException('Error saving the object.');
        }
    }
}