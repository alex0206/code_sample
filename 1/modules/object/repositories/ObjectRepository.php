<?php


namespace backend\modules\object\repositories;


use backend\modules\object\models\Object as ObjectModel;
use common\exceptions\NotFoundException;
use RuntimeException;
use yii\helpers\ArrayHelper;

class ObjectRepository
{
    private $cacheStorage = [];

    /**
     * @param int $id PK
     * @return ObjectModel
     * @throws NotFoundException
     */
    public function findOne(int $id): ObjectModel
    {
        $model = $this->getCacheValue($id);

        if (empty($model)) {
            /** @var ObjectModel $model */
            $model = ObjectModel::findOne($id);

            if (!$model) {
                throw new NotFoundException('The object is not found with id: ' . $id);
            }

            $this->setCacheValue($id, $model);
        }

        return $model;
    }

    /**
     * @param int $id PK
     * @return array
     * @throws NotFoundException
     */
    public function getClientInfo(int $id): array
    {
        $clientInfo = ObjectModel::find()
            ->where(['id' => $id])
            ->select([
                'client_name',
                'client_phone',
                'client_email',
                'client_birthday',
            ])
            ->asArray()
            ->limit(1)
            ->one();

        if (!$clientInfo) {
            throw new NotFoundException('The client data of object with id: ' . $id . ' is not found');
        }

        return $clientInfo;
    }

    /**
     * @param ObjectModel $model
     * @throws RuntimeException
     */
    public function save(ObjectModel $model)
    {
        if ($model->save(false) === false) {
            throw new RuntimeException('Error saving the object.');
        }
    }

    private function getCacheValue($key)
    {
        return ArrayHelper::getValue($this->cacheStorage, $key);
    }

    private function setCacheValue($key, $value)
    {
        $this->cacheStorage[$key] = $value;
    }
}