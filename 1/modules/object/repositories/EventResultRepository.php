<?php


namespace backend\modules\object\repositories;


use backend\modules\object\models\ObjectEventResult;
use RuntimeException;

class EventResultRepository
{
    /**
     * @param ObjectEventResult $model
     * @throws RuntimeException
     */
    public function save(ObjectEventResult $model)
    {
        if ($model->save(false) === false) {
            throw new RuntimeException('Error saving the object.');
        }
    }
}